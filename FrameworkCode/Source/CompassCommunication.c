/**************************************************
Module
  CompassCommunication.c
 
Description
  For communication with the COMPASS
 
**************************************************/
 
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
// the common headers for C99 types
#include <stdint.h>
#include <stdbool.h>
 
//from IO Module
#include <stdio.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_pwm.h"
#include "inc/hw_timer.h"
#include "inc/hw_nvic.h"
#include "inc/hw_ssi.h"
 
// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"
#include "driverlib/pwm.h"
 
//ES related
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"
#include "ES_Timers.h"
#include "ES_Port.h"
#include "ES_Events.h"
 
#include "BITDEFS.H"
#include "termio.h"
 
#include "CompassCommunication.h"
 
 
/*----------------------------- Module Defines ----------------------------*/

#define TEAM_TO_REGISTER NORTH

#define CPSDVSR 90
#define SCR 30
#define BitsPerNibble 4
#define QueryPeriod 20  //20ms period
#define EMPTY 0x00
#define NORTH 0x10
#define SOUTH 0x01
#define TEAMINFO 0xD2
#define ECOPOINTS 0xB4
#define GAME_STATUS 0x78
#define VALUE_OF_RECYCLE 0x69
#define FREQ_MASK (BIT7HI | BIT6HI | BIT5HI | BIT4HI)
#define COLOR_MASK (BIT3HI | BIT2HI | BIT1HI)
#define TEAM_MASK BIT0HI
#define WESTRECYCLE_MASK (BIT7HI | BIT6HI | BIT5HI)
#define EASTRECYCLE_MASK (BIT4HI | BIT3HI | BIT2HI)
#define GAMESTATE_MASK (BIT1HI | BIT0HI)
 
/******************************* Module Variables ***************************/
static uint8_t ReceivedData1;
static uint8_t ReceivedData2;
static uint8_t ReceivedData3;
static uint8_t ReceivedData4;
static uint8_t ReceivedData5;
static uint8_t ReceivedData6;
static uint8_t RawData;
static uint8_t CompassCommand;
static uint8_t MyPriority;
static CompassState_t CurrentState;
static uint8_t CommandCount = 0;
static char WhichTeam;
static uint32_t WhatPeriod;
static char WhatAssignedColor;
static uint32_t NorthScore;
static uint32_t SouthScore;
static char WestRecyclingColor;
static char EastRecyclingColor;
static char GameState;
static uint8_t RecycleValue;
static uint32_t InterruptTest = 0;
static uint8_t DataCount = 0;
static uint8_t IsDataReady = 0;



/*----------------------------- Module Code ---------------------------*/
/***********************************************************************
Function
  InitCompassCommunication
 
Parameters
  uint8_t Priority
 
Returns
  bool, false if error in initialization, true otherwise
 
Description
  Runs the initialization routine for the CompassCommunication service
 
Author
  Hojung Choi
*************************************************************************/
 
bool InitCompassCommunication(uint8_t Priority){
  ES_Event_t ThisEvent;
  MyPriority = Priority;
  
  // Enable the clock to the GPIO port (port A)
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R0;
  // Enable the clock to SSI module. SSI module 0
  HWREG(SYSCTL_RCGCSSI) |= SYSCTL_RCGCSSI_R0; 
  // Wait for the GPIO port A to be ready
  while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R0) != SYSCTL_PRGPIO_R0)
    {;}
  // Program the GPIO to use the alternate functions on the SSI pins. PA2, PA3, PA4, PA5
  HWREG(GPIO_PORTA_BASE + GPIO_O_AFSEL) |= (BIT2HI | BIT3HI | BIT4HI | BIT5HI);
  // Set mux position in GPIOPCTL to select the SSI use of the pins. bitspernibble is 4
  HWREG(GPIO_PORTA_BASE + GPIO_O_PCTL) =
      (HWREG(GPIO_PORTA_BASE + GPIO_O_PCTL) & 0xff0000ff) + (2 << (2 * BitsPerNibble)) + (2 << (3 * BitsPerNibble)) +
      (2 << (4 * BitsPerNibble)) + (2 << (5 * BitsPerNibble));
  // Program the port lines for digital I/O
  HWREG(GPIO_PORTA_BASE + GPIO_O_DEN) |= (BIT2HI | BIT3HI | BIT4HI | BIT5HI);
  // Program the required data directions on the port lines
  // Clock out, Data Out, Data In, Frame Select output
  HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) |= (BIT2HI | BIT3HI | BIT5HI);
  HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) &= (BIT4LO);
  // If using SPI mode 3, program the pull-up on the clock line
  HWREG(GPIO_PORTA_BASE + GPIO_O_PUR) = 1;
  // Wait for the SS1I0 to be ready
  while ((HWREG(SYSCTL_PRSSI) & SYSCTL_PRSSI_R0) != SYSCTL_PRSSI_R0) {}
  // Make sure that the SSI is disabled before programming mode bits
  HWREG(SSI0_BASE + SSI_O_CR1) &= (~SSI_CR1_SSE);
  // Select master mode (MS) & TXRIS indicating End of Transmit (EOT)
  // Select master
  HWREG(SSI0_BASE + SSI_O_CR1) &= (~SSI_CR1_MS);
  // Configure end of transmission
  HWREG(SSI0_BASE + SSI_O_CR1) |= SSI_CR1_EOT;
  // Configure the SSI clock source to the system clock
  HWREG(SSI0_BASE + SSI_O_CC) &= (~SSI_CC_CS_M);    
  // We want to set our clock to 15kHz-ish, but lower than 15kHz
  // SysInClk = SysClk / (CPSDVR * (1*SCR)) = 40MHz / (20 * (1 + 134))
  // Configure the clock pre-scaler CPSDVR
  HWREG(SSI0_BASE + SSI_O_CPSR) = HWREG(SSI0_BASE + SSI_O_CPSR) & (0xFFFFFF00) + CPSDVSR;
  // Configure SSI Control Register - set clock rate (SCR) to 134
  HWREG(SSI0_BASE + SSI_O_CR0) = (HWREG(SSI0_BASE + SSI_O_CR0) & (~SSI_CR0_SCR_M)) + (SCR << 8);
  // Configure SSI Control Register - phase & polarity (SPH, SPO), 
  // SPH = 1, SPO = 1          |= makes it 1
  HWREG(SSI0_BASE + SSI_O_CR0) |= SSI_CR0_SPH;
  HWREG(SSI0_BASE + SSI_O_CR0) |= SSI_CR0_SPO;
  // Configure SSI Control Register - mode (FRF) is Freescale SPI mode
  HWREG(SSI0_BASE + SSI_O_CR0) = ((HWREG(SSI0_BASE + SSI_O_CR0) & (~SSI_CR0_FRF_M)) | SSI_CR0_FRF_MOTO);
  // Configure SSI Control Register - DSS is 8 bit data size
  HWREG(SSI0_BASE + SSI_O_CR0) = ((HWREG(SSI0_BASE + SSI_O_CR0) & (~SSI_CR0_DSS_M)) | SSI_CR0_DSS_8);
  // Locally enable interrupts (TXIM in SSIIM)
  HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;                      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  //HWREG(SSI0_BASE + SSI_O_IM) &= (~SSI_IM_TXIM);                     //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  // Make sure that the SSI is enabled for operation
  HWREG(SSI0_BASE + SSI_O_CR1) |= SSI_CR1_SSE;
  // Enable the NVIC interrupt for the SSI when starting to transmit
  // SSI0 Interrupt is Interrupt #7
  HWREG(NVIC_EN0) |= BIT7HI;
  // set SSI0 Interrupt to lower priority
  // HWREG(NVIC_PRI1) |= (3 << 29); // wrong!
  // Global Init Interrupt
  __enable_irq();

  puts("CompassCommunication initialize complete.\r\n");

  CurrentState = TEAM_REGISTERING;
  ThisEvent.EventType = ES_INIT;
  ES_Timer_InitTimer(QueryTimer, QueryPeriod);
  
  return ES_PostToService(MyPriority, ThisEvent);
}
 
/***************************************************************************
Function
  PostCompassCommunicationService
Parameter
  ES_Event_t ThisEvent
Returns
  bool
***************************************************************************/
bool PostCompassCommunicationService( ES_Event_t ThisEvent ){
  return ES_PostToService(MyPriority, ThisEvent);
}
 
/***************************************************************************
Function
  RunCompassCommunicationService
Parameter
  ES_Event_t ThisEvent
Returns
  ES_Event_t
****************************************************************************/
ES_Event_t RunCompassCommunicationService( ES_Event_t ThisEvent ){
  static ES_Event_t ReturnValue;
  ReturnValue.EventType = ES_NO_EVENT;

//  printf("\r\nReceivedData1 is.. %x", ReceivedData1);
//  printf("\r\nReceivedData2 is.. %x", ReceivedData2);
//  printf("\r\nReceivedData3 is.. %x", ReceivedData3);
//  printf("\r\nReceivedData4 is.. %x", ReceivedData4);
//  printf("\r\nReceivedData5 is.. %x", ReceivedData5);
//  printf("\r\nReceivedData6 is.. %x", ReceivedData6);
//
//  printf("\r\nInterruptCount is.. %d", InterruptTest);
//

  switch(CurrentState){
	case TEAM_REGISTERING:{
    //printf("\r\nInterrupt Test is.. %d", InterruptTest);
    // try incorporating everything into the interrupt response.. we are more sure with the timing that way..
    if( (ThisEvent.EventType == ES_TIMEOUT) && (CommandCount == 0) ){
      //ReceivedData4 = CompassCommand;
      //printf("\r\n ReceivedData4 is.. %x", ReceivedData4); 
      Compass_Write( TEAM_TO_REGISTER); //depending on team assignment
      Compass_Write( EMPTY);
      Compass_Write( EMPTY);
      CommandCount ++;
      ES_Timer_InitTimer(QueryTimer, QueryPeriod);
      //printf("\r\nInterruptCount is.. %d", InterruptTest);
    }else if( (ThisEvent.EventType == ES_TIMEOUT) && (CommandCount == 1) ){
      HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;
 

      CommandCount = 0; 
      ProcessData_REGISTER();

      ES_Timer_InitTimer(QueryTimer, QueryPeriod);
//      printf("\r\nInterruptCount is.. %d", InterruptTest);
    } 




//    else if( (ThisEvent.EventType == ES_TIMEOUT) && (CommandCount == 1) ){
//      //ReceivedData3 = CompassCommand;
//      printf("\r\n ReceivedData3 is.. %x", ReceivedData3); 
//      HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;
//      //Compass_Write( EMPTY);
//      CommandCount ++;
//      ES_Timer_InitTimer(QueryTimer, QueryPeriod);
//    } else if( (ThisEvent.EventType == ES_TIMEOUT) && (CommandCount == 2) ){
//      //ReceivedData2 = CompassCommand;
//      printf("\r\n ReceivedData2 is.. %x", ReceivedData2); 
//      HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;
//      //Compass_Write( EMPTY);
//      CommandCount ++;
//      ES_Timer_InitTimer(QueryTimer, QueryPeriod);
//    } else if( (ThisEvent.EventType == ES_TIMEOUT) && (CommandCount == 3) ){
//			//ReceivedData1 = CompassCommand;
//      printf("\r\n ReceivedData1 is.. %x", ReceivedData1); 
//      HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;
//      CommandCount = 0; 
//      ProcessData_REGISTER();
//      ES_Timer_InitTimer(QueryTimer, QueryPeriod);
//    } 
    
//        if( ThisEvent.EventType == QUERY_READY ){
//        //Query. Write the query command
//        SPI_Write(QUERYINPUT);
//        //start query timer
//        ES_Timer_InitTimer(QueryTimer, QueryPeriod);
//        //change state to waiting
//        CurrentState = Waiting;
//        printf("\r\nCommand: %d ", Command);
    
	} break;
	
	case GETTING_TEAM_INFO:{
//    printf("\r\n WhichTeam: %c", WhichTeam);
//    printf("\r\n WhatPeriod: %d", WhatPeriod);
//    printf("\r\n WhatAssignedColor: %c", WhatAssignedColor);
//    printf("\r\n NorthScore: %x", NorthScore);
//    printf("\r\n SouthScore: %x", SouthScore);
//    printf("\r\n WestRecyclingColor: %c", WestRecyclingColor);
//    printf("\r\n EastRecyclingColor: %c", EastRecyclingColor);
//    printf("\r\n GameState: %c", GameState);
//    printf("\r\n RecycleValue: %x", RecycleValue);
    

    if( (ThisEvent.EventType == ES_TIMEOUT) && (CommandCount == 0) ){
      Compass_Write( TEAMINFO); //depending on team assignment
      Compass_Write( EMPTY);
      Compass_Write( EMPTY);
      CommandCount ++;
      ES_Timer_InitTimer(QueryTimer, QueryPeriod);
    } else if( (ThisEvent.EventType == ES_TIMEOUT) && (CommandCount == 1) ){
      //ReceivedData1 = CompassCommand;
			HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;
      CommandCount = 0;
      ProcessData_TEAMINFO();
      ES_Timer_InitTimer(QueryTimer, QueryPeriod);
    } 


//    else if( (ThisEvent.EventType == ES_TIMEOUT) && (CommandCount == 1) ){
//			HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;
//      Compass_Write( EMPTY);
//      CommandCount ++;
//      ES_Timer_InitTimer(QueryTimer, QueryPeriod);
//    } else if( (ThisEvent.EventType == ES_TIMEOUT) && (CommandCount == 2) ){
//			HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;
//      Compass_Write( EMPTY);
//      CommandCount ++;
//      ES_Timer_InitTimer(QueryTimer, QueryPeriod);
//    } else if( (ThisEvent.EventType == ES_TIMEOUT) && (CommandCount == 3) ){
//      ReceivedData1 = CompassCommand;
//			HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;
//      CommandCount = 0;
//      ProcessData_TEAMINFO();
//      ES_Timer_InitTimer(QueryTimer, QueryPeriod);
//    } 
    
	}break;
  
  case GETTING_ECO_POINTS:{
    
    if( (ThisEvent.EventType == ES_TIMEOUT) && (CommandCount == 0) ){
      Compass_Write( ECOPOINTS); 
      Compass_Write( EMPTY);
      Compass_Write( EMPTY);
      Compass_Write( EMPTY);
      Compass_Write( EMPTY);
      Compass_Write( EMPTY);
      CommandCount ++;
      ES_Timer_InitTimer(QueryTimer, QueryPeriod);
    }else if( (ThisEvent.EventType == ES_TIMEOUT) && (CommandCount == 1) ){
			HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;
      CommandCount = 0;
      ProcessData_ECOPOINTS();
      ES_Timer_InitTimer(QueryTimer, QueryPeriod);
    } 
    
    
  }break;
  
  case GETTING_GAME_STATUS:{
    
    if( (ThisEvent.EventType == ES_TIMEOUT) && (CommandCount == 0) ){
      Compass_Write( GAME_STATUS); //depending on team assignment
      Compass_Write( EMPTY);
      Compass_Write( EMPTY);
      CommandCount ++;
      ES_Timer_InitTimer(QueryTimer, QueryPeriod);
    }else if( (ThisEvent.EventType == ES_TIMEOUT) && (CommandCount == 1) ){
			HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;
      CommandCount = 0;
      ProcessData_GAMESTATUS();
      ES_Timer_InitTimer(QueryTimer, QueryPeriod);
    } 
    
  }break;
  
  case GETTING_VALUE_OF_ITEMS:{
    
    if( (ThisEvent.EventType == ES_TIMEOUT) && (CommandCount == 0) ){
      Compass_Write( VALUE_OF_RECYCLE); 
      CommandCount ++;
      ES_Timer_InitTimer(QueryTimer, QueryPeriod);
    } else if( (ThisEvent.EventType == ES_TIMEOUT) && (CommandCount == 1) ){
			HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;
      Compass_Write( EMPTY);
      CommandCount ++;
      ES_Timer_InitTimer(QueryTimer, QueryPeriod);
    } else if( (ThisEvent.EventType == ES_TIMEOUT) && (CommandCount == 2) ){
			HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;
      Compass_Write( EMPTY);
      CommandCount ++;
      ES_Timer_InitTimer(QueryTimer, QueryPeriod);
    } else if( (ThisEvent.EventType == ES_TIMEOUT) && (CommandCount == 3) ){
      ReceivedData1 = CompassCommand;
			HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;
      CommandCount = 0;
      ProcessData_RECYCLEVALUE();
      ES_Timer_InitTimer(QueryTimer, QueryPeriod);
    } 
    
    
  }break;
  
  } 
  return ReturnValue;
}
 
 
 
/***************************************************************************
Function
  	CompassCommunicationResponse
Parameter
  	void
Returns
  	void
****************************************************************************/
void CompassCommunicationResponse(void){
  	HWREG(SSI0_BASE + SSI_O_IM) &= (~SSI_IM_TXIM);
    InterruptTest++;
  
    if( CurrentState == GETTING_ECO_POINTS){
      ReceivedData1 = Compass_Read();
      ReceivedData2 = Compass_Read();
      ReceivedData3 = Compass_Read();
      ReceivedData4 = Compass_Read();
      ReceivedData5 = Compass_Read();
      ReceivedData6 = Compass_Read();
    }else{
      ReceivedData1 = Compass_Read();
      ReceivedData2 = Compass_Read();
      ReceivedData3 = Compass_Read();
    }
  
    //count data..
  
    //if current state is getting score
    //get 6 values
//    if( CurrentState == GETTING_ECO_POINTS){
//      if( DataCount == 0){
//        ReceivedData1 = Compass_Read();
//        DataCount++;
//      }else if( DataCount == 1){
//        ReceivedData2 = Compass_Read();
//        DataCount++;
//      }else if( DataCount == 2){
//        ReceivedData3 = Compass_Read();
//        DataCount++;
//      }else if( DataCount == 3){
//        ReceivedData4 = Compass_Read();
//        DataCount++;
//      }else if( DataCount == 4){
//        ReceivedData5 = Compass_Read();
//        DataCount++;
//      }else if( DataCount == 5){
//        ReceivedData6 = Compass_Read();
//        DataCount = 0;
//        IsDataReady = 1;
//      }
//    }else{
//      if( DataCount == 0){
//        ReceivedData1 = Compass_Read();
//        DataCount++;
//      }else if( DataCount == 1){
//        ReceivedData2 = Compass_Read();
//        DataCount++;
//      }else if( DataCount == 2){
//        ReceivedData3 = Compass_Read();
//        DataCount = 0;
//        IsDataReady = 1;
//      }
//    }
      
      
  
    //else
    //get 3 values
    
 //	ReceivedData1 = Compass_Read();
//    ReceivedData2 = Compass_Read();
//    ReceivedData3 = Compass_Read();
//    ReceivedData4 = Compass_Read();
//    ReceivedData5 = Compass_Read();
//    ReceivedData6 = Compass_Read();
  
//    if(CommandCount == 1){
//      ReceivedData1 = Compass_Read();
//    }else if(CommandCount == 2){
//      ReceivedData2 = Compass_Read();
//    }else if(CommandCount == 3){
//      ReceivedData3 = Compass_Read();
//    }
}
 
/****************************************************************************
Function
    Compass_Read()
Parameter
  	void
Returns
  	Command
*****************************************************************************/
uint8_t Compass_Read(void){
  	RawData = HWREG(SSI0_BASE + SSI_O_DR);
  	return RawData;
}
 
/****************************************************************************
Function
  	SPI_Write()
Parameter
  	Command
Returns
  	void
*****************************************************************************/
void Compass_Write(uint8_t input){
  	HWREG(SSI0_BASE + SSI_O_DR) = input;
}

/****************************************************************************
Function
    ProcessData_REGISTER()
Parameter
    void
Returns
    void
****************************************************************************/
void ProcessData_REGISTER( void){
  //sort values

//    printf("\r\nReceivedData1 is.. %x", ReceivedData1);
//    printf("\r\nReceivedData2 is.. %x", ReceivedData2);
//    printf("\r\nReceivedData3 is.. %x", ReceivedData3);
//  
  
  //if inappropriate, produce error message
  if( (ReceivedData3 & BIT0HI) == 0 ){
    printf("illegal byte format used to register\r\n");
    CurrentState = TEAM_REGISTERING;
  }else if( (ReceivedData3 & BIT0HI) == 1 ){
    //sort teams..
    if( (ReceivedData3 & BIT1HI) == BIT1HI ){
      WhichTeam = 'S';
      CurrentState = GETTING_TEAM_INFO;                         
    }else{
      WhichTeam = 'N';
      CurrentState = GETTING_TEAM_INFO;
    }
  }
	
	ReceivedData1 = 0;
	ReceivedData2 = 0;
	ReceivedData3 = 0;
	ReceivedData4 = 0;
  ReceivedData5 = 0;
  ReceivedData6 = 0;
  
}

/****************************************************************************
Function
    ProcessData_TEAMINFO()
Parameter
    void
Returns
    void
****************************************************************************/
void ProcessData_TEAMINFO( void){
  //sort values
  static uint8_t FrequencyCode;
	static uint8_t ColorCode;
	static uint8_t TeamCode;
	

  
	FrequencyCode = ((ReceivedData3 & FREQ_MASK) >> 4);
	ColorCode = ((ReceivedData3 & COLOR_MASK) >> 1);
	TeamCode = (ReceivedData3 & TEAM_MASK);
  
//  printf("\r\nFrequencyCode: %x", FrequencyCode);
//  printf("\r\nColorCode: %x", ColorCode);
//  printf("\r\nTeamCode: %x", TeamCode);
  

	
  
	//find out period
	if( FrequencyCode == 0x00 ){ 
		WhatPeriod = 1000;
	}else if( FrequencyCode == 0x01 ){
		WhatPeriod = 947;
	}else if( FrequencyCode == 0x02 ){
		WhatPeriod = 893;
	}else if( FrequencyCode == 0x03 ){
		WhatPeriod = 840;
	}else if( FrequencyCode == 0x04 ){
		WhatPeriod = 787;
	}else if( FrequencyCode == 0x05 ){
		WhatPeriod = 733;
	}else if( FrequencyCode == 0x06 ){
		WhatPeriod = 680;
	}else if( FrequencyCode == 0x07 ){
		WhatPeriod = 627;
	}else if( FrequencyCode == 0x08 ){
		WhatPeriod = 573;
	}else if( FrequencyCode == 0x09 ){
		WhatPeriod = 520;
	}else if( FrequencyCode == 0x0A ){
		WhatPeriod = 467;
	}else if( FrequencyCode == 0x0B ){
		WhatPeriod = 413;
	}else if( FrequencyCode == 0x0C ){
		WhatPeriod = 360;
	}else if( FrequencyCode == 0x0D ){
		WhatPeriod = 307;
	}else if( FrequencyCode == 0x0E ){
		WhatPeriod = 253;
	}else if( FrequencyCode == 0x0F ){
		WhatPeriod = 200;
	}
		
	//find out color
	if( ColorCode == 0x00 ){
		WhatAssignedColor = 'R';
	}else if( ColorCode == 0x01 ){
		WhatAssignedColor = 'O';
	}else if( ColorCode == 0x02 ){
		WhatAssignedColor = 'Y';
	}else if( ColorCode == 0x03 ){
		WhatAssignedColor = 'G';
	}else if( ColorCode == 0x04 ){
		WhatAssignedColor = 'B';
	}else if( ColorCode == 0x05 ){
		WhatAssignedColor = 'P';
	}
	
	//find out team
	if( TeamCode == 0x00 ){
		WhichTeam = 'N';
	}else if( TeamCode == 0x01 ){
		WhichTeam = 'S';
	}
	
	CurrentState = GETTING_ECO_POINTS;
	
	ReceivedData1 = 0;
	ReceivedData2 = 0;
	ReceivedData3 = 0;
	ReceivedData4 = 0;
  ReceivedData5 = 0;
  ReceivedData6 = 0;

}

/****************************************************************************
Function
    ProcessData_ECOPOINTS()
Parameter
    void
Returns
    void
****************************************************************************/
void ProcessData_ECOPOINTS( void){
  //sort the values.. bit31 comes in first.. 
  //have two score variables. north and south
  //concatenate values
  NorthScore = (ReceivedData3 << 8) | ReceivedData4;
  SouthScore = (ReceivedData5 << 8) | ReceivedData6;
  
//  printf("\r\nReceivedData1 is.. %x", ReceivedData1);
//    printf("\r\nReceivedData2 is.. %x", ReceivedData2);
//    printf("\r\nReceivedData3 is.. %x", ReceivedData3);
//  printf("\r\nReceivedData4 is.. %x", ReceivedData4);
//    printf("\r\nReceivedData5 is.. %x", ReceivedData5);
//    printf("\r\nReceivedData6 is.. %x", ReceivedData6);
  
  
  CurrentState = GETTING_GAME_STATUS;
  
  ReceivedData1 = 0;
	ReceivedData2 = 0;
	ReceivedData3 = 0;
	ReceivedData4 = 0;
  ReceivedData5 = 0;
  ReceivedData6 = 0;
}

/****************************************************************************
Function
    ProcessData_GAMESTATUS()
Parameter
    void
Returns
    void
****************************************************************************/
void ProcessData_GAMESTATUS( void){
  
  //save each values in separate variables. make sure to perform bitshift
  static uint8_t WEST;
  static uint8_t EAST;
  static uint8_t STATE;
  
  WEST = ((ReceivedData3 & WESTRECYCLE_MASK) >> 5);
  EAST = ((ReceivedData3 & EASTRECYCLE_MASK) >> 2);
  STATE = (ReceivedData3 & GAMESTATE_MASK);
  
    
  
//  printf("\r\n WEST..: %x", WEST);
//  printf("\r\n EAST..: %x", EAST);
//  printf("\r\n STATE..: %x", STATE);
  
  //sort values for west recycling accepted color
  if( WEST == 0x00 ){
    WestRecyclingColor = 'R';
  }else if( WEST == 0x01 ){
    WestRecyclingColor = 'O';
  }else if( WEST == 0x02 ){
    WestRecyclingColor = 'Y';
  }else if( WEST == 0x03 ){
    WestRecyclingColor = 'G';
  }else if( WEST == 0x04 ){
    WestRecyclingColor = 'B';
  }else if( WEST == 0x05 ){
    WestRecyclingColor = 'P';
  }
  
  //sort values for east recycling accepted color
  if( EAST == 0x00 ){
    EastRecyclingColor = 'R';
  }else if( EAST == 0x01 ){
    EastRecyclingColor = 'O';
  }else if( EAST == 0x02 ){
    EastRecyclingColor = 'Y';
  }else if( EAST == 0x03 ){
    EastRecyclingColor = 'G';
  }else if( EAST == 0x04 ){
    EastRecyclingColor = 'B';
  }else if( EAST == 0x05 ){
    EastRecyclingColor = 'P';
  }
  
  //sor values for the game state
  if( STATE == 0x00 ){
    GameState = 'W';
  } else if( STATE == 0x01 ){
    GameState = 'C';
  } else if( STATE == 0x02 ){
    GameState = 'G';
  } else if( STATE == 0x03 ){
    GameState = 'R';
  }
  
  CurrentState = GETTING_VALUE_OF_ITEMS;
  
  ReceivedData1 = 0;
	ReceivedData2 = 0;
	ReceivedData3 = 0;
	ReceivedData4 = 0;
  ReceivedData5 = 0;
  ReceivedData6 = 0;
}

/****************************************************************************
Function
    ProcessData_RECYCLEVALUE()
Parameter
    void
Returns
    void
****************************************************************************/
void ProcessData_RECYCLEVALUE( void){

  RecycleValue = ReceivedData3;
  
//  printf("\r\nReceivedData1 is.. %x", ReceivedData1);
//    printf("\r\nReceivedData2 is.. %x", ReceivedData2);
//    printf("\r\nReceivedData3 is.. %x", ReceivedData3);

  
  CurrentState = GETTING_TEAM_INFO;
  
  ReceivedData1 = 0;
	ReceivedData2 = 0;
	ReceivedData3 = 0;
	ReceivedData4 = 0;
  ReceivedData5 = 0;
  ReceivedData6 = 0;
}

/****************************************************************************
Function
  QueryTeam()
Parameter
  void
Returns
  char WhichTeam
****************************************************************************/
char QueryTeam( void){
  	return WhichTeam;
}

/****************************************************************************
Function
  QueryPeriod()
Parameter
  void
Returns
   uint32_t WhatPeriod
****************************************************************************/
uint32_t QueryPeriods( void){
  	return WhatPeriod;
}

/****************************************************************************
Function
  QueryAssignedColor()
Parameter
  void
Returns
  char WhatAssignedColor
****************************************************************************/
char QueryAssignedColor( void){ 
  	return WhatAssignedColor;
}

/****************************************************************************
Function
  QueryNorthScore()
Parameter
  void
Returns
  uint32_t NorthScore
****************************************************************************/
uint32_t QueryNorthScore( void){ 
  	return NorthScore;
}

/****************************************************************************
Function
  QuerySouthScore()
Parameter
  void
Returns
  uint32_t SouthScore
****************************************************************************/
uint32_t QuerySouthScore( void){ 
  	return SouthScore;
}

/****************************************************************************
Function
  QueryWestRecyclingColor()
Parameter
  void
Returns
  char WestRecyclingColor
****************************************************************************/
char QueryWestRecyclingColor( void){ 
  	return WestRecyclingColor;
}

/****************************************************************************
Function
  QueryEastRecyclingColor()
Parameter
  void
Returns
  char EastRecyclingColor
****************************************************************************/
char QueryEastRecyclingColor( void){ 
  	return EastRecyclingColor;
}

/****************************************************************************
Function
  QueryGameState()
Parameter
  void
Returns
  char GameState
****************************************************************************/
char QueryGameState( void){ 
  	return GameState;
}

/****************************************************************************
Function
  QueryRecycleValue()
Parameter
  void
Returns
  uint8_t RecycleValue
****************************************************************************/
uint8_t QueryRecycleValue( void){ 
  	return RecycleValue;
}

