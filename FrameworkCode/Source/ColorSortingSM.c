/****************************************************************************
 Module
   ColorSortingSM.c

 Revision
   1.0.1

 Description
   This is the first service for the Test Harness under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 10/26/17 18:26 jec     moves definition of ALL_BITS to ES_Port.h
 10/19/17 21:28 jec     meaningless change to test updating
 10/19/17 18:42 jec     removed referennces to driverlib and programmed the
                        ports directly
 08/21/17 21:44 jec     modified LED blink routine to only modify bit 3 so that
                        I can test the new new framework debugging lines on PF1-2
 08/16/17 14:13 jec      corrected ONE_SEC constant to match Tiva tick rate
 11/02/13 17:21 jec      added exercise of the event deferral/recall module
 08/05/13 20:33 jec      converted to test harness service
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/

// Hardware
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// Event & Services Framework
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"
#include "ES_Port.h"

// Our application
#include "I2CService.h"
#include "ColorSortingSM.h"
#include "CompassCommunication.h"
#include "PWMLibrary.h"
#include "ColorSortingSM.h"
#include "TestHarnessService0.h"
#include "MasterHSM.h"

/*----------------------------- Module Defines ----------------------------*/
// these times assume a 1.000mS/tick timing
#define ONE_SEC 1000
#define HALF_SEC (ONE_SEC / 2)
#define TWO_SEC (ONE_SEC * 2)
#define FIVE_SEC (ONE_SEC * 5)
#define COLOR_TIME 150



/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
char ColorSort(void);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static ColorSortingState_t CurrentState;
static uint16_t ClearValue;
static float RedValue;
static float GreenValue;
static float BlueValue;
static char BallColor;
static char AssignedColor;
static uint8_t RecycleBallCount;
static uint8_t LandfillBallCount;
static uint8_t PACMAN_SERVO= 9;
static uint8_t RECYCLE_POSITION = 90;
static uint8_t LANDFILL_POSITION = 10;
static uint8_t RESET_POSITION = 50;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     StartColorSortingSM

 Parameters
		ES_Event_t CurrentEvent

 Returns
    None

 Description
     Does any required initialization for the new state machine

Author
		YangYoSeob lover
****************************************************************************/
void StartColorSortingSM(ES_Event_t CurrentEvent){
	CurrentState = Detecting;
	//printf("In StartColorSortingSM..\r\n");
	ES_Timer_InitTimer(I2C_TEST_TIMER, (COLOR_TIME));
}

//double check the logic.. with the hierarchial structure..
//bool InitColorSortingSM(uint8_t Priority)
//{
//  ES_Event_t ThisEvent;

//  MyPriority = Priority;
//  // post the initial transition event
//  CurrentState = Detecting;
//  ThisEvent.EventType = ES_INIT;
//  ES_Timer_InitTimer(I2C_TEST_TIMER, (COLOR_TIME));
//  if (ES_PostToService(MyPriority, ThisEvent) == true)
//  {
//    return true;
//  }
//  else
//  {
//    return false;
//  }
//}


/****************************************************************************
 Function
    RunTestHarnessI2C

 Parameters
   ES_Event CurrentEvent

 Returns
   ES_Event_t

 Description
   runs ColorSorting

****************************************************************************/
ES_Event_t RunColorSortingSM(ES_Event_t CurrentEvent){
	bool MakeTransition = false;
	ColorSortingState_t NextState = CurrentState;
	ES_Event_t ReturnEvent = CurrentEvent;
	
	switch(CurrentState){
    case Detecting:        // If current state is initial Psedudo State
    {
     // printf("In detecting state \r\n");
      if (CurrentEvent.EventType == ES_TIMEOUT)    // only respond to ES_Init
      {
    //    printf("ES timedout \r\n");
        BallColor = ColorSort();
   //     printf("Ball color is %c\r\n", BallColor);
        bool x;
        if (BallColor == 'C'){
          x = true;
        } else{
          x = false;
        }
      //  printf("are these true? %d \r\n", x);
        if (BallColor != 'C'){
     //     printf("Ball detected \r\n");
          CurrentState = Sorting;
          ES_Event_t ColorBallEvent;
          ColorBallEvent.EventType = COLOR_BALL_PRESENT;
          ColorBallEvent.EventParam = BallColor;
          PostMasterHSM(ColorBallEvent);
        }
        else{
    //      printf("starting timer \r\n");
          ES_Timer_InitTimer(I2C_TEST_TIMER, (COLOR_TIME));
        }
      }
      else if (CurrentEvent.EventType == POOPING){
        CurrentState = Pooping;
      }
    } // end of InitPState processing
    break;
		
		
    case Sorting:
    {
      //printf("In sorting state \r\n");
      if( CurrentEvent.EventType == POOPING){
        CurrentState = Pooping;
      }
      else if (CurrentEvent.EventType == COLOR_BALL_PRESENT){
//      AssignedColor = QueryAssignedColor();
       AssignedColor = 'R'; //why is our assigned color R.........
      if ((BallColor == AssignedColor) || (BallColor == 'B')){
        //printf("Recycle ball \r\n");
        if (RecycleBallCount < 4){
          //turn servo
          PWM_TIVA_SetDuty(RECYCLE_POSITION, PACMAN_SERVO);
          RecycleBallCount += 1;
          //printf("Recycleballcount is %d \r\n", RecycleBallCount);
          ES_Timer_InitTimer(PACMAN_RESET_TIMER, ONE_SEC);
        }
        else{ //QQQQQQQ so we don't turn the servo? just leave the ball there?
          CurrentState = Detecting;
          ES_Timer_InitTimer(I2C_TEST_TIMER, (COLOR_TIME));
        }
      }
      else{
        //printf("Landfill ball \r\n");
        if (LandfillBallCount < 4){
          //turn servo
          PWM_TIVA_SetDuty(LANDFILL_POSITION, PACMAN_SERVO);
          LandfillBallCount += 1;
          //printf("Landfillballcount is %d \r\n", LandfillBallCount);
          ES_Timer_InitTimer(PACMAN_RESET_TIMER, ONE_SEC);
        }
        else{
          CurrentState = Detecting;
          ES_Timer_InitTimer(I2C_TEST_TIMER, (COLOR_TIME));
        }
      }
    }
      else if (CurrentEvent.EventType == ES_TIMEOUT && CurrentEvent.EventParam == PACMAN_RESET_TIMER){
        //printf("Reset pacman \r\n");
        PWM_TIVA_SetDuty(RESET_POSITION, PACMAN_SERVO);
        CurrentState = Detecting;
        ES_Timer_InitTimer(I2C_TEST_TIMER, (COLOR_TIME));
      }
    }
    break;
		
		
		
		
		
		
		
    case Pooping:
    {
      //printf("in pooping \r\n");
      if(CurrentEvent.EventType == ES_POOPED){
        CurrentState = Detecting;
        ES_Timer_InitTimer(I2C_TEST_TIMER, (COLOR_TIME));
				//make the count 0????????????????????
      }
    }
    break;
		
		CurrentEvent.EventType = ES_NO_EVENT;
		return CurrentEvent;
}

   

} // end of InitPState processing
	


/***************************************************************************
 private functions
 ***************************************************************************/
char ColorSort(void){
 // printf("in Color Sort \r\n");
  ClearValue = I2C_GetClearValue();
  RedValue = ((float)I2C_GetRedValue()*100/ClearValue);
  GreenValue = ((float)I2C_GetGreenValue()*100/ClearValue);
  BlueValue = ((float)I2C_GetBlueValue()*100/ClearValue);
 // printf("Clr: %d, R%%: %.2f, G%% %.2f, B%% %.2f \r\n",
        //  ClearValue, RedValue, GreenValue, BlueValue);

  if(RedValue < 67.0f && RedValue > 62.0f && GreenValue < 22.0f && GreenValue > 18.0f && BlueValue < 21.0f && BlueValue > 16.0f){
    return 'R';
  }
  else if(RedValue < 66.0f && RedValue > 60.0f && GreenValue < 24.0f && GreenValue > 20.0f && BlueValue < 18.0f && BlueValue > 13.0f){
    return 'O';
  }
  else if(RedValue < 50.0f && RedValue > 48.0f && GreenValue < 36.0f && GreenValue > 34.0f && BlueValue < 17.0f && BlueValue > 13.0f){
    return 'Y';
  }
  else if(RedValue < 57.0f && RedValue > 53.0f && GreenValue < 24.0f && GreenValue > 20.0f && BlueValue < 28.0f && BlueValue > 24.0f){
    return 'P';
  }
  else if(RedValue < 39.0f && RedValue > 37.0f && GreenValue < 42.0f && GreenValue > 40.0f && BlueValue < 23.0f && BlueValue > 18.0f){
    return 'G';
  }
  else if(RedValue < 21.0f && RedValue > 18.0f && GreenValue < 35.0f && GreenValue > 32.0f && BlueValue < 50.0f && BlueValue > 48.0f){
    return 'B';
  }
  else{
    return 'C';
  }

}
/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

