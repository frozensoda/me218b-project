/*
  IRInputCapture.c
  
  detects period of IR emitter using input capture interrupts
  
  jingya@stanford.edu 02/04/2019
*/


#include "IRInputCapture.h"
#include <stdio.h>

#include "BITDEFS.H"  
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_timer.h"
#include "inc/hw_nvic.h"

#define US_PER_TICK 0.025f
#define MIN_CONSEC_COUNT 5
#define ERROR 0.02f // percentage error

static uint32_t Period;
static uint32_t LastPeriod;
static uint32_t LastCapture;
static uint16_t ConsecutiveReadingCount;

void InitIRInputCapture() {
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R1;
	while((HWREG(SYSCTL_PRWTIMER) & SYSCTL_PRWTIMER_R1) != SYSCTL_PRWTIMER_R1){};
		
	// enable the clock to Port C
	HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R2;
	// since we added this Port C clock init, we can immediately start
	// into configuring the timer, no need for further delay
	// make sure that timer (Timer A) is disabled before configuring
	HWREG(WTIMER1_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TAEN;
		
	// set it up in 32bit wide (individual, not concatenated) mode
	// the constant name derives from the 16/32 bit timer, but this is a 32/64
	// bit timer so we are setting the 32bit mode
	HWREG(WTIMER1_BASE+TIMER_O_CFG) = TIMER_CFG_16_BIT;
		
	// we want to use the full 32 bit count, so initialize the Interval Load
	// register to 0xffffffff (its default value :-)
	HWREG(WTIMER1_BASE+TIMER_O_TAILR) = 0xffffffff;
		
	// set up timer A in capture mode (TAMR=3, TAAMS = 0),
	// for edge time (TACMR = 1) and up-counting (TACDIR = 1)
	HWREG(WTIMER1_BASE+TIMER_O_TAMR) =
					(HWREG(WTIMER1_BASE+TIMER_O_TAMR) & ~TIMER_TAMR_TAAMS) |
					(TIMER_TAMR_TACDIR | TIMER_TAMR_TACMR | TIMER_TAMR_TAMR_CAP);
	
	// To set the event to capture the rising edge, we need to modify the TAEVENT bits
	// in GPTMCTL. 
	HWREG(WTIMER1_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TAEVENT_M;

	// Now Set up the port to do the capture (clock was enabled earlier)
	// start by setting the alternate function for Port C bit 6 (WT1CCP0)
	HWREG(GPIO_PORTC_BASE+GPIO_O_AFSEL) |= BIT6HI;
	
	// Then, map bit 4's alternate function to WT1CCP0
	// 7 is the mux value to select WT1CCP0, 24 to shift it over to the
	// right nibble for bit 6 (4 bits/nibble * 6 bits)
	HWREG(GPIO_PORTC_BASE+GPIO_O_PCTL) =
	(HWREG(GPIO_PORTC_BASE+GPIO_O_PCTL) & 0xf0ffffff) + (7<<24);

	// Enable pin on Port C for digital I/O
	HWREG(GPIO_PORTC_BASE+GPIO_O_DEN) |= BIT6HI;

	// make pin 6 on Port C into an input
	HWREG(GPIO_PORTC_BASE+GPIO_O_DIR) &= BIT6LO; 
	
	// back to the timer to enable a local capture interrupt
	HWREG(WTIMER1_BASE+TIMER_O_IMR) |= TIMER_IMR_CAEIM;
	
	// enable the Timer A in Wide Timer 1 interrupt in the NVIC
	// it is interrupt number 96 so appears in EN3 at bit 0
	HWREG(NVIC_EN3) |= BIT0HI;
	
	// make sure interrupts are enabled globally
	__enable_irq();
		
	
	// now kick the timer off by enabling it and enabling the timer to
	// stall while stopped by the debugger
	HWREG(WTIMER1_BASE+TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL);
	puts("IR Input capture initialized.\r\n");
}


void IRInputCaptureHandler() {
  // clear source of interrupt
  HWREG(WTIMER1_BASE+TIMER_O_ICR) = TIMER_ICR_CAECINT;

  uint32_t ThisCapture = HWREG(WTIMER1_BASE+TIMER_O_TAR);
  uint32_t ThisPeriod = ThisCapture - LastCapture;
	LastCapture = ThisCapture;

  if (ThisPeriod <= LastPeriod * (1 + ERROR) && ThisPeriod >= LastPeriod * (1 - ERROR)) {
  	ConsecutiveReadingCount ++;
  }
  else {
  	ConsecutiveReadingCount = 0;
  }
  LastPeriod = ThisPeriod;
}

float GetLastIRPeriodUs() {
  return Period * US_PER_TICK;
}

uint32_t GetFrequencyHz() {
  return 40000000/Period;
}

void SetIRPeriod(uint32_t NewPeriod) {
  Period = NewPeriod;
}

bool DoesPeriodMatch (float PeriodToMatch, float Tolerance) {
	float PeriodInUs = Period * US_PER_TICK;
	if (PeriodInUs <= PeriodToMatch * (1 + Tolerance) && PeriodInUs >= PeriodToMatch * (1 - Tolerance)
		&& ConsecutiveReadingCount >= MIN_CONSEC_COUNT) {
		printf("Period %f matches %f\r\n and %d consecutive readings were read.\r\n",
						PeriodInUs, PeriodToMatch, ConsecutiveReadingCount);
		return true;
	}
	return false;
}