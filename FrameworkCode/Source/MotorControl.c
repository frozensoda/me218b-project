/*
  MotorControl.c

  controls the speed of the motors using PID control
  uses SM structure to ignore all function calls until the previous command has finished executing

  02/25/19 ckyj@stanford.edu
*/

#include "MotorControl.h"
#include "ES_Framework.h"
#include "PWMLibrary.h"
#include <math.h>

// access to hardware
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_timer.h"
#include "inc/hw_nvic.h"
#include "inc/hw_pwm.h"

#define TICKS_PER_MS 40000
#define CONTROL_PERIOD 10 // in ms
// encoder CPR = 12, gearbox ratio = 50
#define PULSES_PER_REV 150
#define ANGLE_EPSILON 1
#define K_I 0.25
#define K_P 0.6
#define BASE_RPM 50
#define FAST_RPM BASE_RPM * 1.5f
#define WHEEL_WIDTH 23 // in cm
#define WHEEL_RAD 3.8f

#define PI 3.14159265f

static uint8_t MyPriority;
static float TargetRPMs[] = {0.0f, 0.0f};
static MotorState CurrentState;
static uint32_t LeftLastCapture;
static uint32_t RightLastCapture;
static uint32_t InputCapturePeriods[2];
static Motion DefaultMotion;
static float CurrentRPMs[] = {0.0f, 0.0f};
static int16_t DutyCycles[2];
static bool IsRotatingLeft;
static bool AtCorner;
// static float RPMError;

bool InitMotorControl(uint8_t Priority)
{
  MyPriority = Priority;

  // init GPIO ports for motor control
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1;
  while((!HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R1));
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= BIT0HI | BIT1HI;
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= BIT0HI | BIT1HI;

  // enable clock to wide timer 0, 5 & port C
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R0;
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R5;
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R2;
  // disable timer A and timer B
  HWREG(WTIMER0_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEN & ~TIMER_CTL_TBEN;
  HWREG(WTIMER5_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TBEN;
  // set up 32-bit wide mode
  HWREG(WTIMER0_BASE + TIMER_O_CFG) = TIMER_CFG_16_BIT;
  HWREG(WTIMER5_BASE + TIMER_O_CFG) = TIMER_CFG_16_BIT;
  // set rollover to max value
  HWREG(WTIMER0_BASE + TIMER_O_CFG) = 0xFFFFFFFF;
  HWREG(WTIMER5_BASE + TIMER_O_CFG) = 0xFFFFFFFF;
  // select input capture mode
  HWREG(WTIMER0_BASE + TIMER_O_TAMR) = (HWREG(WTIMER0_BASE + TIMER_O_TAMR) &
          ~TIMER_TAMR_TAAMS) | (TIMER_TAMR_TACDIR | TIMER_TAMR_TACMR | TIMER_TAMR_TAMR_CAP);
  HWREG(WTIMER0_BASE + TIMER_O_TBMR) = (HWREG(WTIMER0_BASE + TIMER_O_TBMR) &
          ~TIMER_TBMR_TBAMS) | (TIMER_TBMR_TBCDIR | TIMER_TBMR_TBCMR | TIMER_TBMR_TBMR_CAP);
  // set capture event to rising edge
  HWREG(WTIMER0_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEVENT_M & ~TIMER_CTL_TBEVENT_M;
  // set up alternate function for PC4 & PC5
  HWREG(GPIO_PORTC_BASE + GPIO_O_AFSEL) |= BIT4HI | BIT5HI;
  HWREG(GPIO_PORTC_BASE + GPIO_O_PCTL) = (HWREG(GPIO_PORTC_BASE + GPIO_O_PCTL) & 0xFF00FFFF) +
    (7 << 4*4) + (7 << 4*5);
  // set PC4 & PC5 to digital input
  HWREG(GPIO_PORTC_BASE + GPIO_O_DEN) |= BIT4HI | BIT5HI;
  HWREG(GPIO_PORTC_BASE + GPIO_O_DIR) &= BIT4LO & BIT5LO;

  // lower priority (interrupt 94 & 95)
 //  HWREG(NVIC_PRI23) |= ((1 << 29) + (1 << 21));
  // enable NVIC for timer A and B
  HWREG(NVIC_EN2) |= (BIT30HI | BIT31HI);

  // select periodic mode for WTIMER 5B
  HWREG(WTIMER5_BASE + TIMER_O_TBMR) = (HWREG(WTIMER5_BASE + TIMER_O_TBMR) &
          ~TIMER_TBMR_TBAMS) | TIMER_TBMR_TBMR_PERIOD;
  // set timer B timeout
  HWREG(WTIMER5_BASE + TIMER_O_TBILR) = TICKS_PER_MS * CONTROL_PERIOD;
  // set priority of WTIMER5B (interrupt 105) to lower priority
  // HWREG(NVIC_PRI26) |= 2 << 13;
  // enable NVIC for WTIMER5B
  HWREG(NVIC_EN3) |= BIT9HI;

  // enable local interrupts
  HWREG(WTIMER0_BASE + TIMER_O_IMR) |= (TIMER_IMR_CAEIM | TIMER_IMR_CBEIM);
  HWREG(WTIMER5_BASE + TIMER_O_IMR) |= TIMER_IMR_TBTOIM;

  // enable global interrupts
  __enable_irq();
  // enable timers A & B and allow timers to stall when stopped by debugger
  HWREG(WTIMER0_BASE + TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL |
    TIMER_CTL_TBEN | TIMER_CTL_TBSTALL);
  HWREG(WTIMER5_BASE + TIMER_O_CTL) |= TIMER_CTL_TBEN | TIMER_CTL_TBSTALL;
  
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R4;
  while((!HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R4));
  HWREG(GPIO_PORTE_BASE + GPIO_O_DEN) |= BIT0HI | BIT1HI;
  HWREG(GPIO_PORTE_BASE + GPIO_O_DIR) |= BIT0HI | BIT1HI;
  HWREG(GPIO_PORTE_BASE + GPIO_O_DATA + ALL_BITS) &= BIT0LO;

  CurrentState = WaitingForCommand;
  DefaultMotion = Stopped;
  puts("MotorControl initialized.\r\n");

  ES_Event_t InitEvent = {ES_INIT, 0};
  return ES_PostToService(MyPriority, InitEvent);
}

bool PostMotorControl(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

ES_Event_t RunMotorControl(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT;

  if (CurrentState == WaitingForCommand) {
		if (DefaultMotion == DrivingForward) {
			TargetRPMs[0] = TargetRPMs[1] = BASE_RPM;
		} else {
			TargetRPMs[0] = TargetRPMs[1] = 0;
		}
		HWREG(GPIO_PORTB_BASE + GPIO_O_DATA + ALL_BITS) &= BIT0LO & BIT1LO;
		AtCorner = false;
		
    if (ThisEvent.EventType == ROTATE) {
      if (IsRotatingLeft) {
				TargetRPMs[1] = FAST_RPM;
				printf("ROTATING LEFT BY %d MS\r\n", ThisEvent.EventParam);
			}
			else {
				TargetRPMs[0] = FAST_RPM;
				printf("ROTATING RIGHT BY %d MS\r\n", ThisEvent.EventParam);
			}
			ES_Timer_InitTimer(ROTATE_TIMER, ThisEvent.EventParam);
      CurrentState = Executing;
    }
		PrintRPM();
  }
  else if (CurrentState == Executing) {
    // ignore all rotate commands in this state
    if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == ROTATE_TIMER) {
      puts("Reached desired angle. Going back to default motion.\r\n");
      if (DefaultMotion == DrivingForward) {
        TargetRPMs[0] = TargetRPMs[1] = BASE_RPM;
      }
      else {
        // stopping
        TargetRPMs[0] = TargetRPMs[1] = 0;
      }
			PrintRPM();
      CurrentState = WaitingForCommand;
    }
		else if (ThisEvent.EventType == STOP && !AtCorner) {
			CurrentState = WaitingForCommand;
		}
  }

  return ReturnEvent;
}


// drive forward at an angle - Angle positive is CW, negative is CCW
void DriveForward(float Angle) {
  DefaultMotion = DrivingForward;
  // temporarily before encoder is fully implemented
//  PWM_TIVA_SetDuty(50, Right); // right wheel
//  PWM_TIVA_SetDuty(50, Left); // left wheel

	ES_Event_t Event2Post = {ES_NO_EVENT, 0};
  if (fabsf(Angle) > ANGLE_EPSILON) {
    float TravelDist = 2 * PI * WHEEL_WIDTH * fabsf(Angle)/360;
    float TravelTime = (TravelDist/(2 * PI * WHEEL_RAD))/((FAST_RPM - BASE_RPM)/60000.0f);
		printf("TravelTime: %f\r\n", TravelTime);
    ES_Event_t RotateEvent = {ROTATE, (uint16_t) (TravelTime + 0.5f)};
		Event2Post = RotateEvent;
    if (Angle > 0) {
      IsRotatingLeft = false;
    }
    else {
      IsRotatingLeft = true;
    }
  }
	ES_PostToService(MyPriority, Event2Post);
  // printf("Driving forward at angle %0.2f.\r\n", Angle);
}

// rotate about center of robot - Angle positive is CW, negative is CCW
void Rotate(float Angle) {
  // printf("Rotating by %0.2f degrees.\r\n", Angle);

  DefaultMotion = Stopped;
  // temporarily before encoder is fully implemented
//  PWM_TIVA_SetDuty(0, Right); // right wheel
//  PWM_TIVA_SetDuty(0, Left); // left wheel

  // start off stationary
//  TargetRPMs[0] = TargetRPMs[1] = 0;
//  HWREG(GPIO_PORTB_BASE + GPIO_O_DATA + ALL_BITS) &= BIT0LO & BIT1LO;

  if (fabsf(Angle) > ANGLE_EPSILON) {
    float TravelDist = 2 * PI * WHEEL_WIDTH * fabsf(Angle)/360;
		// printf("TravelDist: %f\r\n", TravelDist);
    float TravelTime = (TravelDist/(2 * PI * WHEEL_RAD))/(FAST_RPM/60000.0f);
		// printf("TravelTime: %f\r\n", TravelTime);
    ES_Event_t RotateEvent = {ROTATE, (uint16_t) (TravelTime + 0.5f)};
    if (Angle > 0) {
      IsRotatingLeft = false;
    }
    else {
			IsRotatingLeft = true;
    }
    ES_PostToService(MyPriority, RotateEvent);
  }

}

void Stop() {
  DefaultMotion = Stopped;
  TargetRPMs[0] = TargetRPMs[1] = 0.0f;
//  PWM_TIVA_SetDuty(0, Right);
//  PWM_TIVA_SetDuty(0, Left);
  HWREG(GPIO_PORTB_BASE + GPIO_O_DATA + ALL_BITS) &= BIT0LO & BIT1LO;
  puts("Stopping.\r\n");
}

void RotateAtCorner(float Angle) {
	ES_Event_t StopEvent = {STOP, 0};
	ES_PostToService(MyPriority, StopEvent);
	
	DefaultMotion = Stopped;
	float TravelDist = 2 * PI * WHEEL_WIDTH * fabsf(Angle)/360;
	// printf("TravelDist: %f\r\n", TravelDist);
	float TravelTime = (TravelDist/(2 * PI * WHEEL_RAD))/(FAST_RPM/60000.0f);
	// printf("TravelTime: %f\r\n", TravelTime);
	ES_Event_t RotateEvent = {ROTATE, (uint16_t) (TravelTime + 0.5f)};
	if (Angle > 0) {
		IsRotatingLeft = false;
	}
	else {
		IsRotatingLeft = true;
	}
	AtCorner = true;
	ES_PostToService(MyPriority, RotateEvent);
}

void RotateLeft() {
  DefaultMotion = Stopped;
//  PWM_TIVA_SetDuty(50, Right); // right wheel
//  PWM_TIVA_SetDuty(50, Left); // left wheel
	TargetRPMs[1]= BASE_RPM;
	TargetRPMs[0] = 0;
  HWREG(GPIO_PORTB_BASE + GPIO_O_DATA + ALL_BITS) &= (BIT0LO & BIT1LO);
  puts("Rotating left on the spot.\r\n");
}


void LeftEncoderHandler(void) {
  // clear source of interrupt
  HWREG(WTIMER0_BASE + TIMER_O_ICR) = TIMER_ICR_CAECINT;
  uint32_t ThisCapture = HWREG(WTIMER0_BASE + TIMER_O_TAR);
  InputCapturePeriods[0] = ThisCapture - LeftLastCapture;
  LeftLastCapture = ThisCapture;
}

void RightEncoderHandler(void) {
  // clear source of interrupt
  HWREG(WTIMER0_BASE + TIMER_O_ICR) = TIMER_ICR_CBECINT;
  uint32_t ThisCapture = HWREG(WTIMER0_BASE + TIMER_O_TBR);
  InputCapturePeriods[1] = ThisCapture - RightLastCapture;
  RightLastCapture = ThisCapture;
}

void PeriodicHandler(void) {
  // clear source of interrupt
  HWREG(WTIMER5_BASE + TIMER_O_ICR) = TIMER_ICR_TBTOCINT;
  static float ErrorSum[] = {0.0f, 0.0f};
  for (uint8_t i=0; i < 2; i++) {
    if (InputCapturePeriods[i] == 0) {
      CurrentRPMs[i] = 0.0f;
    }
    else {
      CurrentRPMs[i] = 60000/((1.f * InputCapturePeriods[i] / TICKS_PER_MS) * PULSES_PER_REV);
    }
    float RPMError = TargetRPMs[i] - CurrentRPMs[i];
    ErrorSum[i] += RPMError;
    float RequestedDuty = (K_P * (RPMError + K_I * ErrorSum[i]));
    // anti-windup
    if (RequestedDuty >= 100) {
      ErrorSum[i] -= RPMError;
      RequestedDuty = 100.0f;
    } else if (RequestedDuty == 0) {
      ErrorSum[i] -= RPMError;
      RequestedDuty = 0.0f;
    } 
    DutyCycles[i] = (uint8_t) (RequestedDuty+0.5);
    if (i) {
      PWM_TIVA_SetDuty(DutyCycles[i], Right);
    }
    else {
      PWM_TIVA_SetDuty(DutyCycles[i], Left);
    }
	}
}

void PrintRPM( void){
	for (uint8_t i = 0; i<2; i++) {
    printf("%dth Encoder period in ticks: %d\r\n", i, InputCapturePeriods[i]);
		printf("TargetRPM[%d] is %f\r\n", i, TargetRPMs[i]);
		printf("CurrentRPM[%d] is %f\r\n", i, CurrentRPMs[i]);
    // printf("RPMError is %f\r\n", RPMError);
    printf("RequestedDuty[%d] is %d\r\n\r\n", i, DutyCycles[i]);
	}
}