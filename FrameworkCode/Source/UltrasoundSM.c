/*
  UltrasoundSM.c
  
  determines distances read by ultrasonic sensors when a READ_DIST event is posted
  
  02/15/19 ckyj@stanford.edu
  02/17/19 juliadi@stanford.edu
*/

#include <DriveHSM.h>
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"

// access to hardware
#include "driverlib/sysctl.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_timer.h"
#include "inc/hw_nvic.h"
#include "inc/hw_pwm.h"

#include "UltrasoundSM.h"
#include "MasterHSM.h"

typedef struct {
    uint32_t RisingEdge;
    uint32_t FallingEdge;
} SensorData;

/*---------------------------------- Module Variables ---------------------------------*/
#define TRIGGER_TIME 10 // in us
#define TICK_TO_DIST ((1. / SysCtlClockGet()) * (34000 / 2)) // in cm
// distance = time in s * speed of sound (340 m/s), divide by 2 because to wall and back



static uint8_t MyPriority;
static SensorData LeftSensorData;
static SensorData RightSensorData;
static SensorDistances Distances;
static UltrasoundState CurrentState;


/*------------------------------------ Module Code ------------------------------------*/

bool InitUltrasoundSM(uint8_t Priority) {
  MyPriority = Priority;
  CurrentState = SendingSignal;

  // enable clock to wide timer 2 & wide timer 3
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R2;
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R3;
  
  // enable the clock to Port D
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R3;
  
  // disable timer A and timer B 
  HWREG(WTIMER2_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEN & ~TIMER_CTL_TBEN;
  HWREG(WTIMER3_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEN & ~TIMER_CTL_TBEN;

  // set up 32-bit wide mode
  HWREG(WTIMER2_BASE + TIMER_O_CFG) = TIMER_CFG_16_BIT;
  HWREG(WTIMER3_BASE + TIMER_O_CFG) = TIMER_CFG_16_BIT;

  // set rollover to max value
  HWREG(WTIMER2_BASE + TIMER_O_CFG) = 0xFFFFFFFF;
  HWREG(WTIMER3_BASE + TIMER_O_CFG) = 0xFFFFFFFF;

  // select input capture mode
  HWREG(WTIMER2_BASE + TIMER_O_TAMR) = (HWREG(WTIMER2_BASE + TIMER_O_TAMR) & 
    ~TIMER_TAMR_TAAMS) | (TIMER_TAMR_TACDIR | TIMER_TAMR_TACMR | TIMER_TAMR_TAMR_CAP);
  HWREG(WTIMER2_BASE + TIMER_O_TBMR) = (HWREG(WTIMER2_BASE + TIMER_O_TBMR) & 
    ~TIMER_TBMR_TBAMS) | (TIMER_TBMR_TBCDIR | TIMER_TBMR_TBCMR | TIMER_TBMR_TBMR_CAP);
  HWREG(WTIMER3_BASE + TIMER_O_TAMR) = (HWREG(WTIMER3_BASE + TIMER_O_TAMR) & 
    ~TIMER_TAMR_TAAMS) | (TIMER_TAMR_TACDIR | TIMER_TAMR_TACMR | TIMER_TAMR_TAMR_CAP);
  HWREG(WTIMER3_BASE + TIMER_O_TBMR) = (HWREG(WTIMER3_BASE + TIMER_O_TBMR) & 
    ~TIMER_TBMR_TBAMS) | (TIMER_TBMR_TBCDIR | TIMER_TBMR_TBCMR | TIMER_TBMR_TBMR_CAP);

  // set capture event to both edges
  HWREG(WTIMER2_BASE + TIMER_O_CTL) |= TIMER_CTL_TAEVENT_M | TIMER_CTL_TBEVENT_M;
  HWREG(WTIMER3_BASE + TIMER_O_CTL) |= TIMER_CTL_TAEVENT_M | TIMER_CTL_TBEVENT_M;

  // set alternate function for PD0 - PD3
  HWREG(GPIO_PORTD_BASE + GPIO_O_AFSEL) |= BIT0HI | BIT1HI | BIT2HI | BIT3HI;
  HWREG(GPIO_PORTD_BASE + GPIO_O_PCTL) = (HWREG(GPIO_PORTD_BASE + GPIO_O_PCTL) 
    & 0xFFFF0000) + 7 + (7<<4) + (7<<8) + (7<<12);

  // set PD0 - PD3 to digital input
  HWREG(GPIO_PORTD_BASE + GPIO_O_DEN) |= BIT0HI | BIT1HI| BIT2HI | BIT3HI;
  HWREG(GPIO_PORTD_BASE + GPIO_O_DIR) &= BIT0LO & BIT1LO & BIT2LO & BIT3LO;

  // enable NVIC
  HWREG(NVIC_EN3) = BIT2HI | BIT3HI | BIT4HI | BIT5HI;

  // enable global interrupts
  __enable_irq();

  // enable timers and allow timers to stall when stopped by debugger
  HWREG(WTIMER2_BASE + TIMER_O_CTL) |= TIMER_CTL_TAEN | TIMER_CTL_TASTALL |
    TIMER_CTL_TBEN | TIMER_CTL_TBSTALL;
  HWREG(WTIMER3_BASE + TIMER_O_CTL) |= TIMER_CTL_TAEN | TIMER_CTL_TASTALL |
    TIMER_CTL_TBEN | TIMER_CTL_TBSTALL;

  // initialize trigger line and have it default to LO
  HWREG(GPIO_PORTD_BASE + GPIO_O_DEN) |= BIT6HI;
  HWREG(GPIO_PORTD_BASE + GPIO_O_DIR) |= BIT6HI;
  HWREG(GPIO_PORTD_BASE + GPIO_O_DATA + ALL_BITS) &= BIT6LO;

  puts("UltrasoundSM Initialized.\r\n");

  // initialize framework short timer
  ES_ShortTimerInit(MyPriority, SHORT_TIMER_UNUSED);

  // post ES_INIT event
  ES_Event_t InitEvent;
  InitEvent.EventType = ES_INIT;
  return ES_PostToService(MyPriority, InitEvent);
}

bool PostUltrasoundSM(ES_Event_t ThisEvent) {
  return ES_PostToService(MyPriority, ThisEvent);
}

ES_Event_t RunUltrasoundSM(ES_Event_t ThisEvent) {
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT;
  static uint8_t SideSensorsActive;

  if (CurrentState == SendingSignal) {
    if (ThisEvent.EventType == READ_DIST) {
      // set trigger line HI
      HWREG(GPIO_PORTD_BASE + GPIO_O_DATA + ALL_BITS) |= BIT6HI;
      ES_ShortTimerStart(TIMER_A, TRIGGER_TIME);
      if (ThisEvent.EventParam == FrontSensors) {
        // clear interrupt latch
        HWREG(WTIMER2_BASE + TIMER_O_ICR) = TIMER_ICR_CAECINT;
        HWREG(WTIMER2_BASE + TIMER_O_ICR) = TIMER_ICR_CBECINT;
        // enable local interrupt for front sensors
        HWREG(WTIMER2_BASE + TIMER_O_IMR) |= TIMER_IMR_CAEIM | TIMER_IMR_CBEIM;
        SideSensorsActive = 0;
      }
      else {
        // clear interrupt latch
        HWREG(WTIMER3_BASE + TIMER_O_ICR) = TIMER_ICR_CAECINT;
        HWREG(WTIMER3_BASE + TIMER_O_ICR) = TIMER_ICR_CBECINT;
        // enable local interrupt for side sensors
        HWREG(WTIMER3_BASE + TIMER_O_IMR) |= TIMER_IMR_CAEIM | TIMER_IMR_CBEIM;
        SideSensorsActive = 1;
      }
    } 
    else if (ThisEvent.EventType == ES_SHORT_TIMEOUT && 
      ThisEvent.EventParam == TIMER_A) {
      // pull trigger line LO
      HWREG(GPIO_PORTD_BASE + GPIO_O_DATA + ALL_BITS) &= BIT6LO;
      // only count readings starting from this point onwards
      LeftSensorData = (SensorData) {0, 0};
      RightSensorData = (SensorData) {0, 0};
      CurrentState = WaitingForResponse;
      ES_PostToService(MyPriority, ReturnEvent);
    }
  }
  else if (CurrentState == WaitingForResponse) {
    if (LeftSensorData.FallingEdge == 0 || RightSensorData.FallingEdge == 0) {
      // no response yet, check again later
      ES_PostToService(MyPriority, ReturnEvent);
    }
    else {
      // echo signal detected on both sensors
      Distances.Left = (LeftSensorData.FallingEdge - LeftSensorData.RisingEdge) * TICK_TO_DIST;
      Distances.Right = (RightSensorData.FallingEdge - RightSensorData.RisingEdge) * TICK_TO_DIST;
      // printf("Left distance: %.2f, Right distance: %.2f\r\n", Distances.Left, Distances.Right);

      // disable interrupts
      HWREG(WTIMER2_BASE + TIMER_O_IMR) &= ~TIMER_IMR_CAEIM & ~TIMER_IMR_CBEIM;
      HWREG(WTIMER3_BASE + TIMER_O_IMR) &= ~TIMER_IMR_CAEIM & ~TIMER_IMR_CBEIM;

      ES_Event_t Event2Post = {DIST_DETECTED, SideSensorsActive};
      PostMasterHSM(Event2Post);
      CurrentState = SendingSignal;
    }
  }
  return ReturnEvent;
}

void InputCaptureHandler1() {
  // clear source of interrupt
  HWREG(WTIMER2_BASE + TIMER_O_ICR) = TIMER_ICR_CAECINT;
  // read value of current edge
  uint32_t CurrentEdge = HWREG(WTIMER2_BASE + TIMER_O_TAR);
  if (LeftSensorData.RisingEdge != 0) {
    LeftSensorData.FallingEdge = CurrentEdge;
  }
  else {
    LeftSensorData.RisingEdge = CurrentEdge;
  }
}

void InputCaptureHandler2() {
  // clear source of interrupt
  HWREG(WTIMER2_BASE + TIMER_O_ICR) = TIMER_ICR_CBECINT;
  // read value of current edge
  uint32_t CurrentEdge = HWREG(WTIMER2_BASE + TIMER_O_TBR);
  if (RightSensorData.RisingEdge != 0) {
    RightSensorData.FallingEdge = CurrentEdge;
  }
  else {
    RightSensorData.RisingEdge = CurrentEdge;
  }
}

void InputCaptureHandler3() {
  // clear source of interrupt
  HWREG(WTIMER3_BASE + TIMER_O_ICR) = TIMER_ICR_CAECINT;
  // read value of current edge
  uint32_t CurrentEdge = HWREG(WTIMER3_BASE + TIMER_O_TAR);
  if (LeftSensorData.RisingEdge != 0) {
    LeftSensorData.FallingEdge = CurrentEdge;
  }
  else {
    LeftSensorData.RisingEdge = CurrentEdge;
  }
}

void InputCaptureHandler4() {
  // clear source of interrupt
  HWREG(WTIMER3_BASE + TIMER_O_ICR) = TIMER_ICR_CBECINT;
  // read value of current edge
  uint32_t CurrentEdge = HWREG(WTIMER3_BASE + TIMER_O_TBR);
  if (RightSensorData.RisingEdge != 0) {
    RightSensorData.FallingEdge = CurrentEdge;
  }
  else {
    RightSensorData.RisingEdge = CurrentEdge;
  }
}

SensorDistances GetDistances() {
  return Distances;
}
