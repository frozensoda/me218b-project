/*
  DriveHSM.c
  
  executes BARGE driving strategy

  02/19/19 ckyj@stanford.edu
*/

#include "ES_Configure.h"
#include "ES_Framework.h"
#include "DriveHSM.h"
#include "UltrasoundSM.h"
#include "MotorControl.h"
#include <math.h>


/*---------------------------------- Module Variables ---------------------------------*/
#define WALL_DIST 20
#define MAX_DIST_DIFF 2
#define WALL_DIST_TOLERANCE 5
#define EPSILON 2
#define ANGLE_STEP_SIZE 10 // angle to rotate when too far/close to wall
#define CORNER_ANGLE 90 // angle to rotate at corner
#define QUERY_DIST_TIME 100 // must be greater than 60 ms
// 7.75'' side, 7.271'' front dist between two ultrasound sensors
#define SIDE_SENSOR_DIST 19.685f
#define PI 3.14159265

static DriveState CurrentState;
static float LeftDistHist[5];
static uint8_t LeftDistHistCounter;
static ReadDistParam LastReadDistParam;

/*------------------------------------ Module Code ------------------------------------*/

static ES_Event_t DuringDrivingToWall(ES_Event_t CurrentEvent) {
  ES_Event_t ReturnEvent = CurrentEvent;
  if (CurrentEvent.EventType == ES_ENTRY) {
    puts("Executing entry actions for DrivingToWall state...\r\n");
    DriveForward(0);
    ES_Timer_InitTimer(QUERY_DIST_TIMER, QUERY_DIST_TIME);
  } else if (CurrentEvent.EventType == ES_EXIT) {
    puts("Executing exit actions for DrivingToWall state...\r\n");
    Stop();
  }
  return ReturnEvent;
}

static ES_Event_t DuringAligningWithWall(ES_Event_t CurrentEvent) {
  ES_Event_t ReturnEvent = CurrentEvent;
  if (CurrentEvent.EventType == ES_ENTRY) {
    puts("Executing entry actions for AligningWithWall state...\r\n");
    RotateLeft();
    for (uint8_t i=0; i < sizeof(LeftDistHist) / sizeof(float); i++) {
      LeftDistHist[i] = 240.0f;
    }
    LeftDistHistCounter = 0;
    ES_Timer_InitTimer(QUERY_DIST_TIMER, QUERY_DIST_TIME);
  } else if (CurrentEvent.EventType == ES_EXIT) {
    puts("Executing exit actions for AligningWithWall state...\r\n");
    Stop();
  }
  return ReturnEvent;
}

static ES_Event_t DuringWallFollowing(ES_Event_t CurrentEvent) {
  ES_Event_t ReturnEvent = CurrentEvent;
  if (CurrentEvent.EventType == ES_ENTRY) {
    puts("Executing entry actions for WallFollowing state...\r\n");
    DriveForward(0);
    ES_Timer_InitTimer(QUERY_DIST_TIMER, QUERY_DIST_TIME);
    LastReadDistParam = FrontSensors;
  } else if (CurrentEvent.EventType == ES_EXIT) {
    puts("Executing exit actions for WallFollowing state...\r\n");
    Stop();
  }
  return ReturnEvent;
}

ES_Event_t RunDriveHSM(ES_Event_t CurrentEvent) {
  bool MakeTransition = false;
  DriveState NextState = CurrentState;
  ES_Event_t ReturnEvent = CurrentEvent;
  ES_Event_t EntryEvent = {ES_ENTRY, 0};

  switch(CurrentState) {
    case DrivingToWall: {
      ReturnEvent = CurrentEvent = DuringDrivingToWall(CurrentEvent);
      switch (CurrentEvent.EventType) {
        case ES_TIMEOUT: {
          if (CurrentEvent.EventParam == QUERY_DIST_TIMER) {
            ES_Event_t ReadDistEvent = {READ_DIST, FrontSensors};
            PostUltrasoundSM(ReadDistEvent);
            // posting READ_DIST event will cause ultrasonic sensors to update
            ES_Timer_InitTimer(QUERY_DIST_TIMER, QUERY_DIST_TIME);
          }
          break;
        }
        case DIST_DETECTED: { // this event posted back when ultrasonic sensors receive signal back
          if (CurrentEvent.EventParam == SideSensors) {
            puts("ERROR: reading from side sensors instead of front.\r\n");
            break;
          }
          SensorDistances Distances = GetDistances();
					// printf("Distance Left: %f, Right: %f\r\n", Distances.Left, Distances.Right);
          if (Distances.Left < WALL_DIST || Distances.Right < WALL_DIST) {
            puts("Wall detected in front of robot.\r\n");
            NextState = AligningWithWall;
            MakeTransition = true;
          }
					PrintRPM();
          break;
        }
          // this case is only for debugging, delete later
        case WALL_DETECTED: {
          NextState = AligningWithWall;
          MakeTransition = true;
          break;
        }
      }
      break;
    }

    case AligningWithWall: {
      ReturnEvent = CurrentEvent = DuringAligningWithWall(CurrentEvent);
      switch (CurrentEvent.EventType) {
        case ES_TIMEOUT: {
          if (CurrentEvent.EventParam == QUERY_DIST_TIMER) {
            ES_Event_t ReadDistEvent = {READ_DIST, SideSensors};
            PostUltrasoundSM(ReadDistEvent);
            ES_Timer_InitTimer(QUERY_DIST_TIMER, QUERY_DIST_TIME);
          }
          break;
        }
        case DIST_DETECTED: {
          if (CurrentEvent.EventParam == FrontSensors) {
            puts("ERROR: reading from front sensors instead of side.\r\n");
            break;
          }
          SensorDistances Distances = GetDistances();
					printf("Distance Left: %f, Right: %f\r\n", Distances.Left, Distances.Right);
//          float LeftDistSum = 0.0f;
//          for (uint8_t i = 0; i < sizeof(LeftDistHist) / sizeof(float); i++) {
//            LeftDistSum += LeftDistHist[i];
//          }

          if (fabsf(Distances.Left - Distances.Right) < MAX_DIST_DIFF && Distances.Left < 2 * WALL_DIST) {
            puts("Aligned with wall.\r\n");
            NextState = WallFollowing;
            MakeTransition = true;
          }
//          // if above condition is never satisfied and robot ends up turning away from wall
//          else if ((Distances.Left - LeftDistSum / (sizeof(LeftDistHist) / sizeof(float))) > EPSILON) {
//            printf("Left Dist: %f, LeftDistHist Avg: %f\r\n", Distances.Left, (LeftDistSum / (sizeof(LeftDistHist) / sizeof(float))));
//            puts("Distance to wall is increasing. Stop turning left.\r\n");
//            NextState = WallFollowing;
//            MakeTransition = true;
//          }
          // otherwise continue rotating left

//          // update array of past readings
//          LeftDistHist[LeftDistHistCounter] = Distances.Left;
//          if (++LeftDistHistCounter == sizeof(LeftDistHist) / sizeof(float)) {
//            LeftDistHistCounter = 0;
//          }
          break;
        }

        // this case is only for debugging, delete later
        case WALL_DETECTED: {
           NextState = WallFollowing;
           MakeTransition = true;
          break;
        }
      }
      break;
    }

    case WallFollowing: {
      ReturnEvent = CurrentEvent = DuringWallFollowing(CurrentEvent);
      switch (CurrentEvent.EventType) {
        case ES_TIMEOUT: {
          if (CurrentEvent.EventParam == QUERY_DIST_TIMER) {
            // alternate between reading front sensors and side sensors
            ES_Event_t ReadDistEvent = {READ_DIST, FrontSensors};
            if (LastReadDistParam == FrontSensors) {
              ReadDistEvent.EventParam = SideSensors;
            }
            LastReadDistParam = ReadDistEvent.EventParam;
            PostUltrasoundSM(ReadDistEvent);
            ES_Timer_InitTimer(QUERY_DIST_TIMER, QUERY_DIST_TIME);
          }
          break;
        }
        case DIST_DETECTED: {

          SensorDistances Distances = GetDistances();
					printf("Distance Left: %f, Right: %f\r\n", Distances.Left, Distances.Right);
          if (CurrentEvent.EventParam == SideSensors) {
            float Angle = 0.0f;
            if (Distances.Left > WALL_DIST + WALL_DIST_TOLERANCE) {
              puts("Robot too far from wall.\r\n");
              // rotate by ANGLE_STEP_SIZE until close enough to wall
              Angle = ANGLE_STEP_SIZE; // rotating CW is positive
            }
            else if (Distances.Left < WALL_DIST - WALL_DIST_TOLERANCE) {
              puts("Robot too close to wall.\r\n");
              Angle = -ANGLE_STEP_SIZE; // rotating CCW is negative
            }
            else if (fabsf(Distances.Left - Distances.Right) > WALL_DIST_TOLERANCE) {
              puts("Adjusting robot to be parallel with wall.\r\n");
              Angle = (float) (180 * atan2((double) (Distances.Left - Distances.Right), SIDE_SENSOR_DIST)/PI);
              // sign of atan2 is consistent with our CW + , CCW - convention
            }
            DriveForward(Angle);
          }
          else { // reading from FrontSensors
            if (Distances.Left < WALL_DIST || Distances.Right < WALL_DIST) {
              puts("Approaching wall ahead.\r\n");
              // stationary rotate
              RotateAtCorner(-CORNER_ANGLE); // CCW
            }
          }
          break;
        }
      }
    }
  }

  if (MakeTransition) {
    puts("Changing Drive state.\r\n");
    CurrentEvent.EventType = ES_EXIT;
    RunDriveHSM(CurrentEvent);
    CurrentState = NextState;
    RunDriveHSM(EntryEvent);
  }
  return ReturnEvent;
}

void StartDriveHSM(ES_Event_t CurrentEvent) {
  CurrentState = DrivingToWall;
  RunDriveHSM(CurrentEvent);
}

