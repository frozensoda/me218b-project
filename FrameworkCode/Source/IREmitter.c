/*
  IREmitter.c
  
  02/25/19 jingya@stanford.edu
*/

#include "IREmitter.h"
#include "UltrasoundSM.h"
#include "ES_Configure.h"
#include "ES_Framework.h"

// access to hardware
#include "driverlib/sysctl.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_timer.h"
#include "inc/hw_nvic.h"
#include "inc/hw_pwm.h"
#include "PWMLibrary.h"

/*---------------------------- Module Variables ---------------------------*/
#define MIN_WALL_DIST 30.0 // in cm
#define ROTATE_TIME 1000
static uint8_t MyPriority;


/*------------------------------ Module Code ------------------------------*/
bool InitIREmitter(uint8_t Priority) {
  // enable motor ports
//  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1;
//  while(!(HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R1));
//  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (BIT1HI | BIT2HI | BIT4HI | BIT7HI); 
//  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= (BIT1HI | BIT2HI | BIT4HI | BIT7HI); 
//  HWREG(GPIO_PORTB_BASE + GPIO_O_DATA + ALL_BITS) &= (BIT1LO & BIT2LO & BIT4LO & BIT7LO); 

//  MyPriority = Priority;
//  CurrentState = DrivingToWall;
//  
//  InitPWM();
//  PWM_TIVA_SetDuty(100, 0);
//  PWM_TIVA_SetDuty(90, 1);
//  PWM_TIVA_SetDuty(80, 2);
//  PWM_TIVA_SetDuty(70, 3);
//  PWM_TIVA_SetDuty(60, 4);
//  PWM_TIVA_SetDuty(50, 5);
//  PWM_TIVA_SetDuty(40, 6);
//  PWM_TIVA_SetDuty(30, 7);
//  PWM_TIVA_SetDuty(20, 8);
//  PWM_TIVA_SetDuty(10, 9);
//  puts("Set channel PB4 to 50");

  // post ES_INIT event
  ES_Event_t InitEvent;
  InitEvent.EventType = ES_INIT;
  return ES_PostToService(MyPriority, InitEvent);
}

bool PostIREmitter(ES_Event_t ThisEvent) {
  return ES_PostToService(MyPriority, ThisEvent);
}

ES_Event_t RunIREmitter(ES_Event_t ThisEvent) {
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  
//  if (CurrentState == DrivingToWall) {
//    printf("Driving forward \r\n");
//    DriveForward();
//    if (ThisEvent.EventType == DIST_DETECTED) {
//      printf("dist detected \r\n");
//      float Distance = GetDistance(0);
//      if (Distance < MIN_WALL_DIST) {
//        printf("stopping \r\n");
//        Stop();
//        CurrentState = AligningWithWall;
//      }
//    }
//  }
//  else if (CurrentState == AligningWithWall) {
//    printf("CurrentState is aligning with wall \r\n");
//    if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam==RotateTimer) {
//      printf("Stop Rotating \r\n");
//      Stop();
//      CurrentState = DrivingToWall;
//    } else{
//      printf("Start rotating \r\n");
//      ES_Timer_InitTimer(RotateTimer, ROTATE_TIME);
//      RotateRight();
//    }
//  }
  return ReturnEvent;
}

