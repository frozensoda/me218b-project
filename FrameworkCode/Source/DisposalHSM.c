/*
  DisposalHSM.c
  
  hierarchal state machine for GARBAGE disposal subsystem

  02/19/19 ckyj@stanford.edu
*/

#include "ES_Configure.h"
#include "ES_Framework.h"
#include "DisposalHSM.h"
#include "DriveHSM.h"
#include "IRInputCapture.h"

/*---------------------------------- Module Variables ---------------------------------*/
static DisposalState CurrentState;
extern float RecyclingPeriod;

/*------------------------------------ Module Code ------------------------------------*/
ES_Event_t RunDisposalHSM(ES_Event_t CurrentEvent) {
  bool MakeTransition = false;
  DisposalState NextState = CurrentState;
  ES_Event_t ReturnEvent = CurrentEvent;

  switch(CurrentState) {
    case Driving: {
      // check if aligned with recycling or landfill bin
//      if (DoesPeriodMatch(RecyclingPeriod, 0.02f)) { // is detected IRperiod within 4% of RecyclingPeriod
//        puts("Recycling bin detected.\r\n");
//        //CurrentEvent.EventType = RECYCLING_DETECTED; // change event that is passed down to lower SM
//        // maybe we want to drive forward a specific distance to reach bin?
//        MakeTransition = true;
//        NextState = Recycling;
//      }
//      else if (DoesPeriodMatch(LandfillPeriod, 0.02f)) {
//        puts("Landfill bin detected.\r\n");
//        CurrentEvent.EventType = LANDFILL_DETECTED;
//        MakeTransition = true;
//        NextState = Landfilling;
//      }

      ReturnEvent = CurrentEvent = DuringDriving(CurrentEvent);
      break;
    }

    case Landfilling: {
      // poop
      break;
    }

    case Recycling: {
      // open door and poop
      break;
    }
  }
  if (MakeTransition) {
    CurrentEvent.EventType = ES_EXIT;
    RunDisposalHSM(CurrentEvent);
    CurrentState = NextState;
    RunDisposalHSM(CurrentEvent);

  }
  return ReturnEvent;
}

void StartDisposalHSM(ES_Event_t CurrentEvent) {
  CurrentState = Driving;
  RunDisposalHSM(CurrentEvent);
}

static ES_Event_t DuringDriving(ES_Event_t CurrentEvent) {
  ES_Event_t ReturnEvent = CurrentEvent;
  if (CurrentEvent.EventType == ES_ENTRY) {
    puts("Starting DriveHSM.\r\n");
    StartDriveHSM(CurrentEvent);
  }
  else if (CurrentEvent.EventType == ES_EXIT) {
    RunDriveHSM(CurrentEvent);
  }
  else {
    ReturnEvent = RunDriveHSM(CurrentEvent);
  }
  return ReturnEvent;
}
