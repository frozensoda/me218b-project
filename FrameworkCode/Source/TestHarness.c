/*
  TestHarness.c

  02/20/19 ckyj@stanford.edu
*/

#include "ES_Configure.h"
#include "ES_Framework.h"
#include "TestHarness.h"
#include "MasterHSM.h"
#include "I2CService.h"
#include "PWMLibrary.h"
#include "MotorControl.h"

// access to hardware
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_timer.h"
#include "inc/hw_nvic.h"
#include "inc/hw_pwm.h"

#define  TEST_TIME 1000

static uint8_t MyPriority;

bool InitTestHarness(uint8_t Priority) {
  MyPriority = Priority;
  ES_Event_t InitEvent = {ES_INIT, 0};
  ES_Timer_InitTimer(TEST_TIMER, TEST_TIME);
  return ES_PostToService(MyPriority, InitEvent);
}

bool PostTestHarness(ES_Event_t ThisEvent) {
  return ES_PostToService(MyPriority, ThisEvent);
} 

ES_Event_t RunTestHarness(ES_Event_t ThisEvent) {
  if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == TEST_TIMER) {
//    printf("Clear value: %d\r\n", I2C_GetClearValue());
//    printf("Red value: %d\r\n", I2C_GetRedValue());
//    printf("Green value: %d\r\n", I2C_GetGreenValue());
//    printf("Blue value: %d\r\n", I2C_GetBlueValue());
//    ES_Timer_InitTimer(TEST_TIMER, TEST_TIME);
  }
  ES_Event_t ReturnEvent = {ES_NO_EVENT, 0};
  return ReturnEvent;
}

bool CheckForKeystroke() {
  // puts(".");
  if (kbhit()) {
    char ch = getchar();
    if (ch == 's') {
      puts("posting GAME_START event.\r\n");
      ES_Event_t PostEvent = {GAME_STATUS_CHANGE, CleaningUp};
      PostMasterHSM(PostEvent);
    }
    else if (ch == 'w') {
      puts("posting WALL_DETECTED event.\r\n");
      ES_Event_t PostEvent = {WALL_DETECTED, 0};
      PostMasterHSM(PostEvent);
    }
    else if (ch == 'e') {
      puts("posting GAME_OVER event.\r\n");
      ES_Event_t PostEvent = {GAME_STATUS_CHANGE, GameOver};
      PostMasterHSM(PostEvent);
    }  
		else if (ch == '3') {
			puts("PWM3\r\n");
			PWM_TIVA_SetDuty(70, 0);
      PWM_TIVA_SetDuty(70, 3);
      HWREG(GPIO_PORTB_BASE + GPIO_O_DATA + ALL_BITS) &= BIT0LO & BIT1LO;
		}
		else if (ch == 'r') {
			RotateAtCorner(-90);
			PrintRPM();
		}
    return true;
  }
  return false;
}
