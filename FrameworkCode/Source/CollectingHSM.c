/******************************************************************************
Module
	CollectingHSM.c

Description
	This is the lower level state machine for the IntakeHSM
	
******************************************************************************/

#include "ES_Configure.h"
#include "ES_Framework.h"
#include "IntakeHSM.h"
#include "CollectingHSM.h"
#include "PWMLibrary.h"

#define ELEVATOR_CHANNEL 1
#define INTAKE_CHANNEL 2

/*---------------------------------- Module Variables ---------------------------------*/
static CollectingState CurrentState;

/*------------------------------------ Module Code ------------------------------------*/
ES_Event_t RunCollectingHSM(ES_Event_t CurrentEvent){
	bool MakeTransition = false;
	CollectingState NextState = CurrentState;
	ES_Event_t ReturnEvent = CurrentEvent;
	
	switch(CurrentState){
		case Collecting:{
			CurrentEvent = DuringCollecting(CurrentEvent);
		}break;
	}
	
	// check if it actually should be a no event
	CurrentEvent.EventType = ES_NO_EVENT;
	return CurrentEvent;
}


/************************************************************
Function
	StartCollectingSM

Parameters
	ES_Event_t CurrentEvent

Returns
	None

Description
	Does any required initialization for this new state machine

************************************************************/
void StartCollectingHSM(ES_Event_t CurrentEvent){
	CurrentState = Collecting;
	printf("Setting PWM as 100...\r\n");
	//start elevator dc motor. at pwm 100
	PWM_TIVA_SetDuty(100,  ELEVATOR_CHANNEL);
	//start intake dc motor at pwm 100
	PWM_TIVA_SetDuty(100,  INTAKE_CHANNEL);
}

/************************************************************
Function
	DuringCollecting

Parameters
	ES_Event_t CurrentEvent

Returns
	Event

Description
	runs Collecting

*************************************************************/
static ES_Event_t DuringCollecting(ES_Event_t Event){
	//essentially, it really does not do anything.
	//only stops the motors in case of an exit function
	if(ES_EXIT == Event.EventType){
		printf("Setting PWM as 0..\r\n");
		// stop elevator dc motor. at pwm 0 M0PWM1
		PWM_TIVA_SetDuty(0,  ELEVATOR_CHANNEL);
		// stop intake dc motor. at pwm 0 M0PWM2
		PWM_TIVA_SetDuty(0,  INTAKE_CHANNEL);
		// honestly, it might be better to just use a power MOSFET to 
		// turn on and off the motors.. for full power. 
	}
	
}

