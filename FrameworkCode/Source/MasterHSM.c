/*
  MasterHSM.c
  
  top level state machine for BARGE control

  02/17/19 ckyj@stanford.edu
*/

#include "ES_Configure.h"
#include "ES_Framework.h"
#include "MasterHSM.h"
#include "DisposalHSM.h"
#include "CompassCommunication.h"
#include "PWMLibrary.h"
#include "IntakeHSM.h"
#include "CollectingHSM.h"

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_pwm.h"

#define NORTH_LANDFILL_PERIOD 800
#define SOUTH_LANDFILL_PERIOD 700

/*---------------------------------- Module Variables ---------------------------------*/
static MasterGameState CurrentState;
static uint8_t MyPriority;
static char LastWestRecyclingColor = 0;
static bool IsWestRecycling;
uint16_t RecyclingPeriod; // in us
uint16_t LandfillPeriod; // in us

/*------------------------------------ Module Code ------------------------------------*/
bool InitMasterHSM(uint8_t Priority) {
  // called once on program startup 
  MyPriority = Priority;
  ES_Event_t EntryEvent  = {ES_ENTRY, 0};
  puts("Initializing Master service.\r\n");
  InitPWM();
  StartMasterHSM(EntryEvent);
  return true;
}

bool PostMasterHSM(ES_Event_t ThisEvent) {
  return ES_PostToService(MyPriority, ThisEvent);
}

ES_Event_t RunMasterHSM(ES_Event_t CurrentEvent) {
  bool MakeTransition = false;
  MasterGameState NextState = CurrentState;
  ES_Event_t ReturnEvent = {ES_NO_EVENT, 0}; // always return ES_NO_EVENT for top hsm

  switch(CurrentState) {
    case WaitingToStart: {
      CurrentEvent = DuringWaitingToStart(CurrentEvent);
      if (CurrentEvent.EventType == GAME_STATUS_CHANGE && CurrentEvent.EventParam == CleaningUp) {
        puts("Game start.\r\n");
        NextState = CleaningUp;
        MakeTransition = true;
      }
      break;
    }
    case CleaningUp: {
      CurrentEvent = DuringCleaningUp(CurrentEvent);
      if (CurrentEvent.EventType == GAME_STATUS_CHANGE && CurrentEvent.EventParam == GameOver) {
        puts("Game over.\r\n");
        NextState = GameOver;
        MakeTransition = true;
      }
      break;
    }
    case GameOver: {
      // do nothing
      break;
    }
  }

  if (MakeTransition) {
    puts("Changing Master state. \r\n");
    // execute exit functions for current state
    CurrentEvent.EventType = ES_EXIT;
    RunMasterHSM(CurrentEvent);

    // entry event for new state
    ES_Event_t EntryEvent = {ES_ENTRY, 0};
    CurrentState = NextState;
    RunMasterHSM(EntryEvent);
  }
  return ReturnEvent;
}

void StartMasterHSM(ES_Event_t CurrentEvent) {
  CurrentState = WaitingToStart;
  puts("Starting Master state machine.\r\n");
  RunMasterHSM(CurrentEvent);
}

static ES_Event_t DuringWaitingToStart(ES_Event_t Event) {
  ES_Event_t ReturnEvent = Event;
  if (Event.EventType == ES_ENTRY) {
    puts("Executing entry actions for WaitingToStart state...\r\n");
  }
  else if (Event.EventType == ES_EXIT) {
    puts("Executing exit actions for WaitingToStart state...\r\n");
    char Team = QueryTeam();
    printf("Registered as team: %c\r\n", Team);
    if (Team == 'N') {
      LandfillPeriod = NORTH_LANDFILL_PERIOD;
    }
    else if (Team == 'S') {
      LandfillPeriod = SOUTH_LANDFILL_PERIOD;
    }
    char Color = QueryAssignedColor();
    printf("Assigned color: %c\r\n", Color);
    char WestRecyclingColor = QueryWestRecyclingColor();
    LastWestRecyclingColor = WestRecyclingColor;
    printf("West recycling color: %c\r\n", WestRecyclingColor);
    char EastRecyclingColor = QueryEastRecyclingColor();
    printf("East recycling color: %c\r\n", EastRecyclingColor);
    
    if (WestRecyclingColor == Color) {
      IsWestRecycling = true;
    }
    else if (EastRecyclingColor == Color) {
      IsWestRecycling = false;
    }
    else {
      puts("ERROR: team color does not match either recycling center.\r\n");
    }
    
    RecyclingPeriod = QueryPeriods();
    printf("Recycling activation period is %d\r\n", RecyclingPeriod);
  }
  return ReturnEvent;
}

static ES_Event_t DuringCleaningUp(ES_Event_t Event) {
  ES_Event_t ReturnEvent = Event;
  if (Event.EventType == ES_ENTRY) {
    puts("Executing entry actions for CleaningUp state...\r\n");
    puts("Starting DisposalSM.\r\n");
    StartDisposalHSM(Event);
		printf("Starting IntakeHSM..\r\n");
		StartIntakeHSM(Event);
  }
  else if (Event.EventType == ES_EXIT) {
    puts("Executing exit actions for CleaningUp state...\r\n");
		//shouldn't there be a run disposal hsm here?
		ReturnEvent = RunIntakeHSM(Event);
  }
  else {
    if (Event.EventType == GAME_STATUS_CHANGE && Event.EventParam == GameOver) {
      puts("GAME TIMEOUT event passed down to lower level SMs.\r\n");
    }
    ReturnEvent = RunDisposalHSM(Event);
		ReturnEvent = RunIntakeHSM(Event);
  }
  return ReturnEvent;
}

bool CheckGameStatus () {
  static char LastGameStatus;
  char CurrentGameStatus = QueryGameState();
  if (CurrentGameStatus != LastGameStatus) {
    MasterGameState NextGameState = WaitingToStart;
    if (CurrentGameStatus == 'C') {
      NextGameState = CleaningUp;
    }
    else if (CurrentGameStatus == 'G') {
      NextGameState = GameOver;
    }
    ES_Event_t GameChangeEvent = {GAME_STATUS_CHANGE, NextGameState};
    ES_PostToService(MyPriority, GameChangeEvent);
    LastGameStatus = CurrentGameStatus;
    return true;
  }
  return false;
}

bool CheckRecyclingLocation() {
  char CurrWestRecyclingColor = QueryWestRecyclingColor();
  // if LastWestRecyclingColor is initialized
  if (LastWestRecyclingColor && (CurrWestRecyclingColor != LastWestRecyclingColor)) {
    IsWestRecycling = !IsWestRecycling;
    LastWestRecyclingColor = CurrWestRecyclingColor;
    puts("Recycling bin location changed!\r\n");
    return true;
  }
  return false;
}

bool CheckRecyclingFreq() {
  static uint16_t LastRecyclingPeriod;
  uint16_t CurrRecyclingPeriod = (uint16_t ) QueryPeriods();
  if (LastRecyclingPeriod && (CurrRecyclingPeriod != LastRecyclingPeriod)) {
    RecyclingPeriod = LastRecyclingPeriod = CurrRecyclingPeriod;
    puts("Recycling frequency has changed.\r\n");
    return true;
  }
  return false;
}