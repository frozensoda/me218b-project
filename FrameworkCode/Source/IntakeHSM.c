/******************************************************************************
Module
	IntakeHSM.c

Description
	This is the hierarchial state machine for the intake system. It contains two lower level state machines; 
	intake/elevation and sorting
	
*******************************************************************************/

#include "ES_Configure.h"
#include "ES_Framework.h"
#include "IntakeHSM.h"
#include "CollectingHSM.h"
#include "ColorSortingSM.h"

/*---------------------------------- Module Variables ---------------------------------*/
static IntakeState CurrentState;

/*------------------------------------ Module Code ------------------------------------*/
ES_Event_t RunIntakeHSM(ES_Event_t CurrentEvent){
	bool MakeTransition = false;
	IntakeState NextState = CurrentState;
	ES_Event_t ReturnEvent = CurrentEvent;
	
	switch(CurrentState){
		case Intaking:{
			CurrentEvent = DuringIntake(CurrentEvent);
			
			//also, somehow react to game over
			
		}break;
	}
	
			
	// check if it actually should be a no_event
	CurrentEvent.EventType = ES_NO_EVENT;
	return CurrentEvent;
}

//start function Q- so lower level SMs, start functions work as inits?
// why can't we just have init embedded in start for the top level?
/************************************************************
Function
	StartIntakeSM

Parameters
	ES_Event_t CurrentEvent

Returns
	None

Description
	Does any required initialization for this new state machine

************************************************************/
void StartIntakeHSM(ES_Event_t CurrentEvent){
	printf( "In StartIntakeSM.. \r\n");
	CurrentState = Intaking;
	RunIntakeHSM(CurrentEvent);
}




// during function
/************************************************************
Function
	DuringIntake

Parameters
	ES_Event_t CurrentEvent

Returns
	Event

Description
	Starts and runs the lower level SMs. Collecting and Sorting. 

*************************************************************/
static ES_Event_t DuringIntake(ES_Event_t Event){
	//printf( "In DuringIntake \r\n");
	//processes ES_ENTRY & ES_EXIT events
	if(ES_ENTRY == Event.EventType){
		//printf( "Starting StartCollectingHSM \r\n");
		StartCollectingHSM(Event);
		//printf( "Starting StartColorSortingSM \r\n");
		StartColorSortingSM(Event);
	}else if(ES_EXIT == Event.EventType){
		//printf( "Exiting RunCollectingHSM \r\n");
		RunCollectingHSM(Event);
		//printf( "Exiting RunSortingHSM \r\n");
		RunColorSortingSM(Event);
	}else{
		//printf( "Running RunCollectingHSM\r\n");
		RunCollectingHSM(Event);
		//printf( "Running RunSortingHSM\r\n");
		RunColorSortingSM(Event);
	}
	
	return Event;
}
		

