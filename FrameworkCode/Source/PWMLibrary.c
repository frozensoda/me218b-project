/****************************************************************************
 Module
     PWM.c
 Description
     Implementation file for the 2-channel version of the PWM Library for
     the Tiva
 Notes
     Enable Ports B6 (channel = 0)and B7 (channel = 1) for PWM
 History
 When           Who     What/Why
 -------------- ---     --------
 02/03/19       jdi     Initial pass at 2-channel PWM
 *****************************************************************************/
#include <stdint.h>
#include <stdbool.h>

/* include header files for the framework
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_pwm.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"

#include "PWMLibrary.h"

/*---------------------------- Module Variables ---------------------------*/
#define PeriodInMS 1 // period in millisec
#define PWMTicksPerMS SysCtlClockGet()/( 32 * 1000 )
#define BitsPerNibble 4
#define NUM_CHANNELS 10

static int8_t MaxConfiguredChannel = -1; // init to illegal value

static uint8_t  LocalDuty[NUM_CHANNELS] = {0};
static const uint32_t ChannelToPWM_MOD[NUM_CHANNELS]={
                                      PWM0_BASE,PWM0_BASE,PWM0_BASE,PWM0_BASE,
                                      PWM0_BASE,PWM0_BASE,
                                      PWM1_BASE,PWM1_BASE,PWM1_BASE,PWM1_BASE};
static const uint32_t ChannelToGENOffset[NUM_CHANNELS]={
                          PWM_O_0_GENA,PWM_O_0_GENB,PWM_O_1_GENA,PWM_O_1_GENB,
                          PWM_O_2_GENA,PWM_O_2_GENB,
                          PWM_O_2_GENA,PWM_O_2_GENB,PWM_O_3_GENA,PWM_O_3_GENB};

static const uint32_t ChannelToCMPOffset[NUM_CHANNELS]={
                          PWM_O_0_CMPA,PWM_O_0_CMPB,PWM_O_1_CMPA,PWM_O_1_CMPB,
                          PWM_O_2_CMPA,PWM_O_2_CMPB,
                          PWM_O_2_CMPA,PWM_O_2_CMPB,PWM_O_3_CMPA,PWM_O_3_CMPB};

static const uint32_t ChannelToACTZERO_ONEOffset[NUM_CHANNELS]={
                          PWM_0_GENA_ACTZERO_ONE,PWM_0_GENB_ACTZERO_ONE,PWM_1_GENA_ACTZERO_ONE,PWM_1_GENB_ACTZERO_ONE,
                          PWM_2_GENA_ACTZERO_ONE,PWM_2_GENB_ACTZERO_ONE,
                          PWM_2_GENA_ACTZERO_ONE,PWM_2_GENB_ACTZERO_ONE,PWM_3_GENA_ACTZERO_ONE,PWM_3_GENB_ACTZERO_ONE};

static const uint32_t ChannelToACTZERO_ZEROOffset[NUM_CHANNELS]={
                          PWM_0_GENA_ACTZERO_ZERO,PWM_0_GENB_ACTZERO_ZERO,PWM_1_GENA_ACTZERO_ZERO,PWM_1_GENB_ACTZERO_ZERO,
                          PWM_2_GENA_ACTZERO_ZERO,PWM_2_GENB_ACTZERO_ZERO,
                          PWM_2_GENA_ACTZERO_ZERO,PWM_2_GENB_ACTZERO_ZERO,PWM_3_GENA_ACTZERO_ZERO,PWM_3_GENB_ACTZERO_ZERO};

static const uint32_t ChannelToGENNormal[NUM_CHANNELS]={
  (PWM_0_GENA_ACTCMPAU_ONE | PWM_0_GENA_ACTCMPAD_ZERO ),
  (PWM_0_GENB_ACTCMPBU_ONE | PWM_0_GENB_ACTCMPBD_ZERO ),
  (PWM_1_GENA_ACTCMPAU_ONE | PWM_1_GENA_ACTCMPAD_ZERO ),
  (PWM_1_GENB_ACTCMPBU_ONE | PWM_1_GENB_ACTCMPBD_ZERO ),
  (PWM_2_GENA_ACTCMPAU_ONE | PWM_2_GENA_ACTCMPAD_ZERO ),
  (PWM_2_GENB_ACTCMPBU_ONE | PWM_2_GENB_ACTCMPBD_ZERO ),
  (PWM_2_GENA_ACTCMPAU_ONE | PWM_2_GENA_ACTCMPAD_ZERO ),
  (PWM_2_GENB_ACTCMPBU_ONE | PWM_2_GENB_ACTCMPBD_ZERO ),
  (PWM_3_GENA_ACTCMPAU_ONE | PWM_3_GENA_ACTCMPAD_ZERO ),
  (PWM_3_GENB_ACTCMPBU_ONE | PWM_3_GENB_ACTCMPBD_ZERO )};

static const uint32_t ChannelToLOADffset[NUM_CHANNELS]={
                          PWM_O_0_LOAD,PWM_O_0_LOAD,PWM_O_1_LOAD,PWM_O_1_LOAD,
                          PWM_O_2_LOAD,PWM_O_2_LOAD,
                          PWM_O_2_LOAD,PWM_O_2_LOAD,PWM_O_3_LOAD,PWM_O_3_LOAD};


/*------------------------------ Module Code ------------------------------*/

void InitPWM( void ){ 
  // Enable the clock to the PWM Module (PWM0 and PWM1) 
  HWREG(SYSCTL_RCGCPWM) |= SYSCTL_RCGCPWM_R0;
  HWREG(SYSCTL_RCGCPWM) |= SYSCTL_RCGCPWM_R1;
  
  // Enable the clock to Port B, E, and F
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1; //PortB
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R4; //PortE
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R5; //PortF
  
  // Select the PWM clock as System Clock/32
  HWREG(SYSCTL_RCC) = (HWREG(SYSCTL_RCC) & ~SYSCTL_RCC_PWMDIV_M) |
  (SYSCTL_RCC_USEPWMDIV | SYSCTL_RCC_PWMDIV_32);
  
  // wait couple cycles for clock startup
  while ((HWREG(SYSCTL_PRPWM) & SYSCTL_PRPWM_R0) != SYSCTL_PRPWM_R0);
  while ((HWREG(SYSCTL_PRPWM) & SYSCTL_PRPWM_R1) != SYSCTL_PRPWM_R1);
  
  // disable the PWM while initializing
  HWREG( PWM0_BASE+PWM_O_0_CTL ) = 0; //M0PWM0 and M0PWM1
  HWREG( PWM0_BASE+PWM_O_1_CTL ) = 0; //M0PWM2 and M0PWM3
  HWREG( PWM0_BASE+PWM_O_2_CTL ) = 0; //M0PWM4 and M0PWM5
  HWREG( PWM1_BASE+PWM_O_2_CTL ) = 0; //M1PWM4 and M1PWM5
  HWREG( PWM1_BASE+PWM_O_3_CTL ) = 0; //M1PWM6 and M1PWM7
  
  // program generators to go to 1 at rising compare A/B, 0 on falling compare A/B
  HWREG( PWM0_BASE+PWM_O_0_GENA) = ( PWM_0_GENA_ACTCMPAU_ONE | PWM_0_GENA_ACTCMPAD_ZERO );
  HWREG( PWM0_BASE+PWM_O_0_GENB) = ( PWM_0_GENB_ACTCMPBU_ONE | PWM_0_GENB_ACTCMPBD_ZERO );
  
  HWREG( PWM0_BASE+PWM_O_1_GENA) = ( PWM_1_GENA_ACTCMPAU_ONE | PWM_1_GENA_ACTCMPAD_ZERO );
  HWREG( PWM0_BASE+PWM_O_1_GENB) = ( PWM_1_GENB_ACTCMPBU_ONE | PWM_1_GENB_ACTCMPBD_ZERO );
  
  HWREG( PWM0_BASE+PWM_O_2_GENA) = ( PWM_2_GENA_ACTCMPAU_ONE | PWM_2_GENA_ACTCMPAD_ZERO );
  HWREG( PWM0_BASE+PWM_O_2_GENB) = ( PWM_2_GENB_ACTCMPBU_ONE | PWM_2_GENB_ACTCMPBD_ZERO );
  
  HWREG( PWM1_BASE+PWM_O_2_GENA) = ( PWM_2_GENA_ACTCMPAU_ONE | PWM_2_GENA_ACTCMPAD_ZERO );
  HWREG( PWM1_BASE+PWM_O_2_GENB) = ( PWM_2_GENB_ACTCMPBU_ONE | PWM_2_GENB_ACTCMPBD_ZERO );
  
  HWREG( PWM1_BASE+PWM_O_3_GENA) = ( PWM_3_GENA_ACTCMPAU_ONE | PWM_3_GENA_ACTCMPAD_ZERO );
  HWREG( PWM1_BASE+PWM_O_3_GENB) = ( PWM_3_GENB_ACTCMPBU_ONE | PWM_3_GENB_ACTCMPBD_ZERO );
  
  // Set the PWM period. Since we are counting both up & down, we initialize
  // the load register to 1/2 the desired total period. We will also program
  // the match compare registers to 1/2 the desired high time
  HWREG( PWM0_BASE+PWM_O_0_LOAD) = ((PeriodInMS * PWMTicksPerMS))>>1;
  HWREG( PWM0_BASE+PWM_O_1_LOAD) = ((PeriodInMS * PWMTicksPerMS))>>1;
  HWREG( PWM0_BASE+PWM_O_2_LOAD) = ((PeriodInMS * PWMTicksPerMS))>>1;
  HWREG( PWM1_BASE+PWM_O_2_LOAD) = ((PeriodInMS * PWMTicksPerMS))>>1;
  HWREG( PWM1_BASE+PWM_O_3_LOAD) = ((PeriodInMS * PWMTicksPerMS))>>1;

  // Set the initial Duty cycle on A to 50% by programming the compare value
  // to 1/2 the period to count up (or down). Technically, the value to program
  // should be Period/2 - DesiredHighTime/2, but since the desired high time is 1/2
  // the period, we can skip the subtract
  // HWREG( PWM0_BASE+PWM_O_0_CMPA) = HWREG( PWM0_BASE+PWM_O_0_LOAD )>>1;
  // HWREG( PWM0_BASE+PWM_O_0_CMPB) = HWREG( PWM0_BASE+PWM_O_0_LOAD )>>1;
  
  // enable the PWM outputs
  HWREG( PWM0_BASE+PWM_O_ENABLE) |= (PWM_ENABLE_PWM0EN | PWM_ENABLE_PWM1EN | 
                                     PWM_ENABLE_PWM2EN | PWM_ENABLE_PWM3EN | 
                                     PWM_ENABLE_PWM4EN | PWM_ENABLE_PWM5EN);
  HWREG( PWM1_BASE+PWM_O_ENABLE) |= (PWM_ENABLE_PWM4EN | PWM_ENABLE_PWM5EN |
                                     PWM_ENABLE_PWM6EN | PWM_ENABLE_PWM7EN);
  
  // configure the Port B4, B5, B6 and B7 pins to be PWM outputs
  HWREG(GPIO_PORTB_BASE+GPIO_O_AFSEL) |= (BIT4HI | BIT5HI | BIT6HI | BIT7HI);
  // configure the Port E4, E5 pins to be PWM outputs
  HWREG(GPIO_PORTE_BASE+GPIO_O_AFSEL) |= (BIT4HI | BIT5HI);
  // configure the Port F0, F1, F2 and F3 pins to be PWM outputs
  HWREG(GPIO_PORTF_BASE+GPIO_O_AFSEL) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI);
  
  // now choose to map PWM to those pins, this is a mux value of 4 that we
  // want to use for specifying the function on bits 4, 5, 6 & 7
  HWREG(GPIO_PORTB_BASE+GPIO_O_PCTL) =
  (HWREG(GPIO_PORTB_BASE+GPIO_O_PCTL) & 0x00ffffff) + (4<<(7*BitsPerNibble)) + 
  (4<<(6*BitsPerNibble)) + (4<<(5*BitsPerNibble)) + (4<<(4*BitsPerNibble));
  
  // now choose to map PWM to those pins, this is a mux value of 4 that we
  // want to use for specifying the function on bits 4, 5
  HWREG(GPIO_PORTE_BASE+GPIO_O_PCTL) =
  (HWREG(GPIO_PORTE_BASE+GPIO_O_PCTL) & 0x00ffffff) + (4<<(5*BitsPerNibble)) + 
  (4<<(4*BitsPerNibble));
  
    // now choose to map PWM to those pins, this is a mux value of 5 that we
  // want to use for specifying the function on bits 0, 1, 2 & 3
  HWREG(GPIO_PORTF_BASE+GPIO_O_PCTL) =
  (HWREG(GPIO_PORTF_BASE+GPIO_O_PCTL) & 0x00ffffff) + (5<<(3*BitsPerNibble)) + 
  (5<<(2*BitsPerNibble)) + (5<<(1*BitsPerNibble)) + (5<<(0*BitsPerNibble));
  
  // Enable pins 4, 5, 6 & 7 on Port B for digital I/O and as outputs
  HWREG(GPIO_PORTB_BASE+GPIO_O_DEN) |= (BIT4HI | BIT5HI | BIT6HI | BIT7HI);
  HWREG(GPIO_PORTB_BASE+GPIO_O_DIR) |= (BIT4HI | BIT5HI | BIT6HI | BIT7HI);
  
  // Enable pins 4, 5 on Port E for digital I/O and as outputs
  HWREG(GPIO_PORTE_BASE+GPIO_O_DEN) |= (BIT4HI | BIT5HI);
  HWREG(GPIO_PORTE_BASE+GPIO_O_DIR) |= (BIT4HI | BIT5HI);
  
  // Enable pins 0, 1, 2 & 3 on Port F for digital I/O and as outputs
  HWREG(GPIO_PORTF_BASE+GPIO_O_DEN) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI);
  HWREG(GPIO_PORTF_BASE+GPIO_O_DIR) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI);
  
  // set the up/down count mode, enable the PWM generator and make
  // both generator updates locally synchronized to zero count
  HWREG(PWM0_BASE+ PWM_O_0_CTL) = (PWM_0_CTL_MODE | PWM_0_CTL_ENABLE |
  PWM_0_CTL_GENAUPD_LS | PWM_0_CTL_GENBUPD_LS);
  
  HWREG(PWM0_BASE+ PWM_O_1_CTL) = (PWM_1_CTL_MODE | PWM_1_CTL_ENABLE |
  PWM_1_CTL_GENAUPD_LS | PWM_1_CTL_GENBUPD_LS);
  
  HWREG(PWM0_BASE+ PWM_O_2_CTL) = (PWM_2_CTL_MODE | PWM_2_CTL_ENABLE |
  PWM_2_CTL_GENAUPD_LS | PWM_2_CTL_GENBUPD_LS);
  
  HWREG(PWM1_BASE+ PWM_O_2_CTL) = (PWM_2_CTL_MODE | PWM_2_CTL_ENABLE |
  PWM_2_CTL_GENAUPD_LS | PWM_2_CTL_GENBUPD_LS);
  
  HWREG(PWM1_BASE+ PWM_O_3_CTL) = (PWM_3_CTL_MODE | PWM_3_CTL_ENABLE |
  PWM_3_CTL_GENAUPD_LS | PWM_3_CTL_GENBUPD_LS);
}


/****************************************************************************
 Function
   PWM_TIVA_SetDuty
 Parameters
   None
 Returns
   bool: true if a new event was detected
 Description
   Sample event checker grabbed from the simple lock state machine example
 Notes
   will not compile, sample only
 Author
   J. Di, 02/03/19, 09:48
****************************************************************************/


bool PWM_TIVA_SetDuty( uint8_t dutyCycle, uint8_t channel )
{
  uint32_t DesiredHighTime;
  
  // sanity check
  if (channel > 10) {
    puts("ERROR: channel number out of bounds.\r\n");
    return false;
  }

  // 0 duty cycle
  if (0 == dutyCycle){
    DesiredHighTime = 0;
    HWREG(ChannelToPWM_MOD[channel] + ChannelToGENOffset[channel]) = ChannelToACTZERO_ZEROOffset[channel];
    
  // 100 duty cycle  
  } else if (dutyCycle >= 100) {
    // To program 100% DC, simply set the action on Zero to set the output to ONE
    DesiredHighTime = 100;
    HWREG(ChannelToPWM_MOD[channel] + ChannelToGENOffset[channel]) = ChannelToACTZERO_ONEOffset[channel];

  // other duty cycles
  } else {
    // printf("setting PWM channel %d to %d\r\n", channel, dutyCycle);
    HWREG(ChannelToPWM_MOD[channel] + ChannelToGENOffset[channel]) = ChannelToGENNormal[channel];
    DesiredHighTime = ( PeriodInMS * PWMTicksPerMS * dutyCycle ) / 100;
    HWREG(ChannelToPWM_MOD[channel] + ChannelToCMPOffset[channel]) = (HWREG(ChannelToPWM_MOD[channel]+ChannelToLOADffset[channel])) - ((DesiredHighTime)>>1);
  } 
  return true;
}
