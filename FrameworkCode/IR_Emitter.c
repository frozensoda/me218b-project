/******************************************************
Module
  IR_Emitter.c
 
Description
  This module is used to control the period of the IR pulsing
 
*******************************************************/


/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
// the common headers for C99 types
#include <stdint.h>
#include <stdbool.h>
 
//from IO Module
#include <stdio.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_pwm.h"
#include "inc/hw_timer.h"
#include "inc/hw_nvic.h"
#include "inc/hw_ssi.h"
 
// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"
#include "driverlib/pwm.h"
 
//ES related
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"
#include "ES_Timers.h"
#include "ES_Port.h"
#include "ES_Events.h"
#include "MasterHSM.h"
 
#include "BITDEFS.H"
#include "termio.h"

#include "IR_Emitter.h"


/*----------------------------- Module Level Defines -----------------------------*/
#define TicksPerUS  40//set
#define HowManyUS  500// set
#define PULSE_HI BIT7HI
#define PULSE_LO BIT7LO


/*----------------------------- Module Level Variables -----------------------------*/
static uint8_t PreviousState = 0;
static uint32_t PeriodForTimer = 0;


void InitIREmitter( void ){
	
	//Set PC7 as digital output
	HWREG(SYSCTL_RCGCGPIO) |= BIT2HI;
	while((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R2) != SYSCTL_PRGPIO_R2)
	{}
		
	HWREG(GPIO_PORTC_BASE+GPIO_O_DEN) |= BIT7HI;
	HWREG(GPIO_PORTC_BASE+GPIO_O_DIR) |= BIT7HI;
	
	//Will use WT1CCP1.. WideTimer1 B
	// start by enabling the clock to the timer (Wide Timer 0)
	HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R1; // kill a few cycles to let the clock get going
	while((HWREG(SYSCTL_PRWTIMER) & SYSCTL_PRWTIMER_R1) != SYSCTL_PRWTIMER_R1)
	{
	}
	// make sure that timer (Timer B) is disabled before configuring
	HWREG(WTIMER1_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TBEN;
	// set it up in 32bit wide (individual, not concatenated) mode
	HWREG(WTIMER1_BASE+TIMER_O_CFG) = TIMER_CFG_16_BIT;
	// set up timer A in periodic mode so that it repeats the time-outs
	HWREG(WTIMER1_BASE+TIMER_O_TBMR) =
	(HWREG(WTIMER1_BASE+TIMER_O_TBMR)& ~TIMER_TBMR_TBMR_M)|
	TIMER_TBMR_TBMR_PERIOD;
	// set timeout to 100mS
	HWREG(WTIMER1_BASE+TIMER_O_TBILR) = TicksPerUS * HowManyUS;
	// enable a local timeout interrupt
	HWREG(WTIMER1_BASE+TIMER_O_IMR) |= TIMER_IMR_TBTOIM;
	// enable the Timer B in Wide Timer 1 interrupt in the NVIC
	// it is interrupt number !!!check!!! so appears in ??? at bit ???
	HWREG(NVIC_EN3) |= BIT1HI; 
	// make sure interrupts are enabled globally
	__enable_irq();
	// now kick the timer off by enabling it and enabling the timer to
	// stall while stopped by the debugger
	HWREG(WTIMER1_BASE+TIMER_O_CTL) |= (TIMER_CTL_TBEN |
	TIMER_CTL_TBSTALL);
	
	//start with PC7 LO
	HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) &= PULSE_LO;
}

//Do not forget to add these in the interrupt

//SetIREmitter
//remember that the input period will be twice the value what the timer period should be
void SetIREmitterPeriod( uint32_t PeriodInput ){
	//Reset ILR(?)
	PeriodForTimer = 0.5*PeriodInput;
	HWREG(WTIMER1_BASE+TIMER_O_TBILR) = (TicksPerUS * PeriodForTimer);
}

//Interrupt response..
void IREmitterResponse( void){
	//clear interrupt
	HWREG(WTIMER1_BASE+TIMER_O_ICR) = TIMER_ICR_TBTOCINT;
	//if pulse was high, make pulse low and vice versa
	if(PreviousState == 0){
		HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) |= PULSE_HI;
		PreviousState = 1;
	}else if(PreviousState == 1){
		HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS)) &= PULSE_LO;
		PreviousState = 0;
	}
}



