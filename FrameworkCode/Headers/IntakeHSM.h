#ifndef  INTAKEHSM_H
#define INTAKEHSM_H

#include "ES_Types.h"
#include "ES_Events.h"

typedef enum {
    Intaking
} IntakeState;

ES_Event_t RunIntakeHSM(ES_Event_t CurrentEvent);
void StartIntakeHSM(ES_Event_t CurrentEvent);
static ES_Event_t DuringIntake(ES_Event_t Event);


#endif 