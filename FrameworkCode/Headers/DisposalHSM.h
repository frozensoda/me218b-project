/*
  DisposalHSM.h
  
  hierarchal state machine for GARBAGE disposal subsystem

  02/19/19 ckyj@stanford.edu
*/

#ifndef DISPOSALHSM_H
#define DISPOSALHSM_H

typedef enum {
  Driving,
  Recycling,
  Landfilling
} DisposalState;

ES_Event_t RunDisposalHSM(ES_Event_t CurrentEvent);
void StartDisposalHSM(ES_Event_t CurrentEvent);
static ES_Event_t DuringDriving(ES_Event_t CurrentEvent);

#endif
