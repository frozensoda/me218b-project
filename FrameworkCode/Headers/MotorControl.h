/*
  MotorControl.h

  controls the speed of the motors using PID control

  02/25/19 ckyj@stanford.edu
*/

#ifndef MOTORCONTROL_H
#define MOTORCONTROL_H

#include "ES_Types.h"
#include "ES_Events.h"

typedef enum {
    WaitingForCommand,
    Executing
} MotorState;

typedef enum {
    DrivingForward,
    Stopped
} Motion;

typedef enum {
    Left = 3,
    Right = 0
} Wheel;


bool InitMotorControl(uint8_t MyPriority);
bool PostMotorControl(ES_Event_t ThisEvent);
ES_Event_t RunMotorControl(ES_Event_t ThisEvent);

void DriveForward(float Angle);
void Stop(void);
void RotateLeft(void);
void Rotate(float Angle);
void PrintRPM( void);
void RotateAtCorner(float Angle);

#endif //MOTORCONTROL_H
