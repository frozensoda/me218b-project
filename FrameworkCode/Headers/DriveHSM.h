/*
  DriveHSM.h
  
  executes BARGE driving strategy

  02/19/19 ckyj@stanford.edu
*/


#ifndef DRIVEHSM_H
#define DRIVEHSM_H

#include "ES_Events.h"

typedef enum {
  DrivingToWall,
  AligningWithWall,
  WallFollowing
} DriveState;

typedef enum {
    FrontSensors = 0,
    SideSensors = 1
} ReadDistParam;


ES_Event_t RunDriveHSM(ES_Event_t CurrentEvent);
void StartDriveHSM(ES_Event_t CurrentEvent);
static ES_Event_t DuringDrivingToWall(ES_Event_t CurrentEvent);
static ES_Event_t DuringAligningWithWall(ES_Event_t CurrentEvent);
static ES_Event_t DuringWallFollowing(ES_Event_t CurrentEvent);

#endif
