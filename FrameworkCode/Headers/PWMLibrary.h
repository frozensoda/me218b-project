#ifndef PWMLIBRARY_H
#define PWMLIBRARY_H

/****************************************************************************
 Module
     PWMTiva.h
 Description
     header file to support use of the 2-channel version of the PWM library
     on the Tiva
 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 02/3/19        jdi     Initial pass of 2-channel PWM 
*****************************************************************************/
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include "ES_Events.h"
void InitPWM( void );
bool PWM_TIVA_SetDuty( uint8_t dutyCycle, uint8_t channel );

#endif //_PWM_TIVA_H
