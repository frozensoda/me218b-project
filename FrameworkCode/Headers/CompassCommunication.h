#ifndef COMPASSCOMMUNICATION_H
#define COMPASSCOMMUNICATION_H
 
#include "ES_Types.h"
#include "ES_Events.h"
#include "ES_Configure.h"
 
typedef enum {TEAM_REGISTERING, GETTING_TEAM_INFO, GETTING_ECO_POINTS, GETTING_GAME_STATUS, GETTING_VALUE_OF_ITEMS} CompassState_t;
 

uint8_t Compass_Read(void);
void Compass_Write(uint8_t input);
void ProcessData_REGISTER( void);
void ProcessData_TEAMINFO( void);
void ProcessData_ECOPOINTS( void);
void ProcessData_GAMESTATUS( void);
void ProcessData_RECYCLEVALUE( void);
char QueryTeam( void);
uint32_t QueryPeriods( void);
char QueryAssignedColor( void);
uint32_t QueryNorthScore( void);
uint32_t QuerySouthScore( void);
char QueryWestRecyclingColor( void);
char QueryEastRecyclingColor( void);
char QueryGameState( void);
uint8_t QueryRecycleValue( void);

bool InitCompassCommunication(uint8_t);
bool PostCompassCommunicationService( ES_Event_t);
ES_Event_t RunCompassCommunicationService( ES_Event_t );





//static char WhichTeam;
//static uint32_t WhatPeriod;
//static char WhatAssignedColor;
//static uint32_t NorthScore;
//static uint32_t SouthScore;
//static char WestRecyclingColor;
//static char EastRecyclingColor;
//static uint32_t GameState;
//static uint8_t RecycleValue;



 
#endif





//first, initialize..

//in each state,

//if timeout and count is 0,
//  write
//  read
//  increment count
//  start timer
//if timeout and count is 1,
//  write
//  read
//  increment count
//  start timer
//if timeout and count is 2,
//  write 
//  read
//  increment count
//  start timer
//if timeout and count is last,
//  write
//  read
//  increment count
//  ProcessData_Whichdata
//  ChageState
//  start timer






