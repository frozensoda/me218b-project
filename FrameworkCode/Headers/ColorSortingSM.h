/****************************************************************************

  Header file for Test Harness I2C Service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef COLORSORTINGSM_H
#define COLORSORTINGSM_H

#include <stdint.h>
#include <stdbool.h>

#include "ES_Events.h"

// Public Function Prototypes

void StartColorSortingSM(ES_Event_t CurrentEvent);
ES_Event_t RunColorSortingSM(ES_Event_t ThisEvent);
char ColorSort(void);

typedef enum
{
  Detecting, Sorting, Pooping
}ColorSortingState_t;

#endif /* TestHarnessI2C_H */

