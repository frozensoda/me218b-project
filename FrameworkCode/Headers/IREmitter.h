#ifndef IREMITTER_H
#define IREMITTER_H

#include "ES_Types.h"

void InitIRInputCapture(void);
void IRInputCaptureHandler(void);
uint32_t GetLastIRPeriod(void);
uint32_t GetFrequency(void);
void SetIRPeriod(uint32_t NewPeriod);

#endif
