/*
  InputCapture.h
  
  enables input capture on any 32-bit wide timer and returns the period between edges
  specify which timer to enable and associated port & NVIC in the predefined macros
  
  ckyj@stanford.edu 02/04/2019
*/

#ifndef IRINPUTCAPTURE_H
#define IRINPUTCAPTURE_H

#include "ES_Types.h"

void InitIRInputCapture(void);
void IRInputCaptureHandler(void);
uint32_t GetLastIRPeriod(void);
uint32_t GetFrequency(void);
void SetIRPeriod(uint32_t NewPeriod);
bool DoesPeriodMatch(float Period, float Tolerance);

#endif
