/*
  UltrasoundSM.h
  
  determines distances read by ultrasonic sensors
  
  02/15/19 ckyj@stanford.edu
  02/17/19 juliadi@stanford.edu
*/

#ifndef ULTRASOUNDSM_H
#define ULTRASOUNDSM_H

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include "ES_Events.h"

typedef struct {
    float Left;
    float Right;
} SensorDistances;

typedef enum {SendingSignal, WaitingForResponse} UltrasoundState;
bool InitUltrasoundSM(uint8_t Priority);
bool PostUltrasoundSM(ES_Event_t ThisEvent);
ES_Event_t RunUltrasoundSM(ES_Event_t ThisEvent);
SensorDistances GetDistances(void);

#endif
