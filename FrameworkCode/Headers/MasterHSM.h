/*
  MasterHSM.h
  
  top level state machine for BARGE control

  02/17/19 ckyj@stanford.edu
*/

#ifndef MASTERHSM_H
#define MASTERHSM_H

typedef enum {
  WaitingToStart = 0,
  CleaningUp = 1,
  GameOver = 2
} MasterGameState;


bool InitMasterHSM(uint8_t Priority);
bool PostMasterHSM(ES_Event_t ThisEvent);
ES_Event_t RunMasterHSM(ES_Event_t CurrentEvent);
void StartMasterHSM(ES_Event_t CurrentEvent);
static ES_Event_t DuringWaitingToStart(ES_Event_t Event);
static ES_Event_t DuringCleaningUp(ES_Event_t Event);

bool CheckGameStatus(void);
bool CheckRecyclingFreq(void);
bool CheckRecyclingLocation(void);

#endif
