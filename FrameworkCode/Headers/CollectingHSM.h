#ifndef  COLLECTINGHSM_H
#define COLLECTINGHSM_H

#include "ES_Types.h"
#include "ES_Events.h"

typedef enum {
    Collecting
} CollectingState;

ES_Event_t RunCollectingHSM(ES_Event_t CurrentEvent);
void StartCollectingHSM(ES_Event_t CurrentEvent);
static ES_Event_t DuringCollecting(ES_Event_t Event);




#endif 