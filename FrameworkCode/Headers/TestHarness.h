/*
  TestHarness.h

  02/20/19 ckyj@stanford.edu
*/

#ifndef TESTHARNESS_H
#define TESTHARNESS_H

bool InitTestHarness(uint8_t Priority);
bool PostTestHarness(ES_Event_t ThisEvent);
ES_Event_t RunTestHarness(ES_Event_t ThisEvent);
bool CheckForKeystroke(void);

#endif
