#ifndef IR_EMITTER_H
#define IR_EMITTER_H
 
#include "ES_Types.h"
#include "ES_Events.h"
#include "ES_Configure.h" 

void InitIREmitter( void );
void SetIREmitterPeriod( uint32_t );

#endif

